//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Camera.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of camera
*/
//----------------------------------------------------------------------------------------

#include "Camera.h"

#include <iostream>
#include "Program.h"

extern Program program;

Camera::Camera(int32_t width, int32_t height, glm::vec3 position, glm::vec3 upVector, float vertAngle, float horAngle)
	: m_width(width), m_height(height), m_position(position), m_upVector(upVector), m_verticalAngle(vertAngle), m_horizontalAngle(horAngle)
{

	Rotate(width / 2, height / 2, 0); // Count direction and upvector
	m_viewMatrix = glm::lookAt(m_position, m_position + m_direction, m_upVector);
	m_static = false;

	m_projectionMatrix = glm::perspective(60.0f, (float)m_width / (float)m_height, 0.1f, 100.0f);
}

Camera::~Camera()
{
}

void Camera::Move(float movement, float deltaTime)
{
	if (m_static) return; // if camera is static discard movement

	glm::vec3 tmp(m_position.x + m_direction.x * deltaTime * movement, m_position.y, m_position.z + m_direction.z * deltaTime * movement);

	if (program.SolveCollisions(tmp)) return; // if camera would collide discard movement

	m_position = tmp;

	m_viewMatrix = glm::lookAt(m_position, m_position + m_direction, m_upVector);
}

void Camera::MoveAside(float movement, float deltaTime)
{
	if (m_static) return; // if camera is static discard movement

	glm::vec3 tmp(m_position.x + m_direction.z * deltaTime * movement, m_position.y, m_position.z + -m_direction.x * deltaTime * movement);

	if (program.SolveCollisions(tmp)) return; // if camera would collide discard movement

	m_position = tmp;
	
	m_viewMatrix = glm::lookAt(m_position, m_position + m_direction, m_upVector);
}

void Camera::Rotate(int32_t mouseX, int32_t mouseY, float deltaTime)
{
	float vertDelta = deltaTime * 5.0f * (mouseY - m_height / 2);
	float horDelta = deltaTime  * 5.0f * (mouseX - m_width / 2);

	if (fabs(m_verticalAngle + vertDelta) < maxVerticalAngle)
		m_verticalAngle += vertDelta;

	m_horizontalAngle += horDelta;
	if (m_horizontalAngle > 360) m_horizontalAngle -= 360; // Recount angles to match limits <0, 360)
	else if (m_horizontalAngle < 0) m_horizontalAngle += 360;
	
	float horizontalAngle = glm::radians(m_horizontalAngle);
	float verticalAngle = glm::radians(m_verticalAngle);

	m_direction.x = cosf(horizontalAngle);
	m_direction.y = 0;
	m_direction.z = sinf(horizontalAngle);

	m_upVector = glm::vec3(0, 1, 0);

	glm::vec3 axis = glm::cross(m_direction, m_upVector); // Make perpendicular axis  to direction and up vector
	glm::mat4 rot = glm::rotate(glm::mat4(1.0f), m_verticalAngle, axis); // make rotation matrix

	m_direction = glm::vec3(glm::vec4(m_direction, 0.0f) * rot); // rotate direction with given matrix
	m_upVector = glm::vec3(glm::vec4(m_upVector, 0.0f) * rot); // roatte up vector with given matrix

	m_viewMatrix = glm::lookAt(m_position, m_position + m_direction, m_upVector);
}

const glm::mat4 Camera::GetViewMatrix(glm::vec3 position) const
{
	return glm::lookAt(position, position + m_direction, m_upVector);
}

void Camera::SetSize(int32_t width, int32_t height)
{
	m_width = width;
	m_height = height;
	m_projectionMatrix = glm::perspective(60.0f, (float)m_width / (float)m_height, 0.1f, 500.0f);
}

void Camera::SetPosition(glm::vec3 position)
{
	m_position = position;
	m_viewMatrix = glm::lookAt(m_position, m_position + m_direction, m_upVector);
}