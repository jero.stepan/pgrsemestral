//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Camera.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of camera
*/
//----------------------------------------------------------------------------------------

#pragma once
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <cstdint>

/// Camera class used for view and projection matrices calculation
class Camera
{
public:
	Camera(int32_t width, int32_t height, glm::vec3 position, glm::vec3 upVector, float vertAngle, float horAngle);
	~Camera();
	/** Move camera in given direction.
	\param amount Amount of the movement (can be positive - move forward, or negative - move backwards)
	\param deltaTime Time elapsed from last frame
	\param bounds World bounds
	*/
	void Move(float amount, float deltaTime);
	/** Move camera in the direction of normal to the camera direction.
	\param amount Amount of the movement (can be positive - move forward, or negative - move backwards)
	\param deltaTime Time elapsed ftom last frame
	\param bounds World bounds
	*/
	void MoveAside(float amount, float deltaTime);
	/** Rotate camera using new position of mouse cursor
	\param mouseX Position of the mouse on X axis
	\param mouseY Position of the mouse on Y axis
	\param deltaTime Time elapsed from last frame
	*/
	void Rotate(int32_t mouseX, int32_t mouseY, float deltaTime);
	/// Toggles static flag (static camera discards movement)
	void ToggleStatic() { m_static = !m_static; }

	/// \return Pprojection matrix
	const glm::mat4& GetProjectionMatrix() const { return m_projectionMatrix; }
	/// \return View matrix
	const glm::mat4& GetViewMatrix() const { return m_viewMatrix; }
	/** Recounts view matrix with given position
	\return View matrix
	*/
	const glm::mat4 GetViewMatrix(glm::vec3 position) const;
	/// \return Direction
	const glm::vec3& GetDirection() const { return m_direction; }
	/// \return Position
	const glm::vec3& GetPosition() const { return m_position; }
	/** Sets a new size of the camera
	\param width New width of the camera
	\param height New height of the camera
	*/
	void SetSize(int32_t width, int32_t height);
	/** Sets a new position of the camera
	\param position New position
	*/
	void SetPosition(glm::vec3 position);

private:
	int32_t m_width; /// Width
	int32_t m_height; /// Height

	glm::vec3 m_position; /// Position
	glm::vec3 m_upVector; /// Up vector

	glm::vec3 m_direction; /// Direction

	glm::mat4 m_projectionMatrix; /// Counted projection matrix
	glm::mat4 m_viewMatrix; /// Counted view matrix

	float m_verticalAngle; /// Vertical angle
	float m_horizontalAngle; /// Horizontal angle
	const float maxVerticalAngle = 45.0f; /// Vertical angle limit
	bool m_static; /// Flag which determines if camera is static or not
};

