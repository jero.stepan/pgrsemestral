//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Controls.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of object Controls
*/
//----------------------------------------------------------------------------------------

#include "Controls.h"

Controls::Controls(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader) : GameObject(position, scale, rotation, shader)
{
	m_shader = static_cast<ShaderControls*>(shader);
	elapsedTime = 0;
}


Controls::~Controls()
{
}

void Controls::Render(const Camera& cam, const std::vector<Light*>& lights, const State& state)
{
	glUseProgram(m_shader->program);

	// Make PVM matrix
	glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), m_position);
	modelMatrix = glm::rotate(modelMatrix, m_rotation.x, glm::vec3(1, 0, 0));
	modelMatrix = glm::rotate(modelMatrix, m_rotation.y, glm::vec3(0, 1, 0));
	modelMatrix = glm::rotate(modelMatrix, m_rotation.z, glm::vec3(0, 0, 1));
	modelMatrix = glm::scale(modelMatrix, m_scale);

	glm::mat4 PVM = cam.GetProjectionMatrix() * cam.GetViewMatrix() * modelMatrix;

	// Uniforms
	m_shader->AssignUniforms(PVM, cam.GetViewMatrix(), modelMatrix, lights, state);
	glUniform1f(m_shader->elapsedTimeLocation, elapsedTime);
	glUniform1f(m_shader->frameTimeLocation, frameTime);
	glUniform2fv(m_shader->patternLocation, 1, glm::value_ptr(pattern));

	std::list<SubMesh>::iterator i = m_submeshes.begin();

	glUniform1i(m_shader->texLocation, 0);
	glActiveTexture(getTextureTarget(0));
	glBindTexture(GL_TEXTURE_2D, i->material.texture);

	// Material structure
	m_shader->AssignMaterial(i->material);

	// Draw
	glBindVertexArray(i->geometry.vertexArrayObject);
	glDrawElements(GL_TRIANGLES, i->geometry.numTriangles * 3, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
	glUseProgram(0);

	CHECK_GL_ERROR();
}

void Controls::Update(float deltaTime)
{
	elapsedTime += deltaTime;
	if (elapsedTime >= frameTime * pattern.x * pattern.y) elapsedTime = 0;
}

bool Controls::Init(const std::string& path, ResourceManager& manager)
{
	SubMesh submesh;

	submesh.material.texture = pgr::createTexture(path);
	if (!submesh.material.texture) {
		std::cerr << "Could not load: " << path << std::endl;
		return false;
	}

	// Generate buffers
	glGenBuffers(1, &submesh.geometry.vertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, submesh.geometry.vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesControls), verticesControls, GL_STATIC_DRAW);

	glGenBuffers(1, &submesh.geometry.elementBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, submesh.geometry.elementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicesControls), indicesControls, GL_STATIC_DRAW);
	
	// Generate VAO and bind VBO and EBO to it
	glGenVertexArrays(1, &submesh.geometry.vertexArrayObject);
	glBindVertexArray(submesh.geometry.vertexArrayObject);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, submesh.geometry.elementBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, submesh.geometry.vertexBufferObject);

	// Enable and set vertex atributes up
	glEnableVertexAttribArray(m_shader->positionLocation);
	glVertexAttribPointer(m_shader->positionLocation, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), 0);
	glEnableVertexAttribArray(m_shader->UVLocation);
	glVertexAttribPointer(m_shader->UVLocation, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(m_shader->normalLocation);
	glVertexAttribPointer(m_shader->normalLocation, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(5 * sizeof(float)));

	CHECK_GL_ERROR();

	glBindVertexArray(0);
	submesh.geometry.numTriangles = sizeof(indicesControls) / (3 * sizeof(unsigned));

	// Set up material
	submesh.material.ambient = glm::vec3(1.0f);
	submesh.material.diffuse = glm::vec3(0.3f);
	submesh.material.specular = glm::vec3(0.3f);
	submesh.material.shininess = 1.0f;

	m_submeshes.push_back(submesh);
	return true;
}