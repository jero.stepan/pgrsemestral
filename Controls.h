//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Controls.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of object Controls
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "GameObject.h"
#include "ShaderControls.h"

// Vertices for Controls game object
const float verticesControls[] = {
	// position			UV			normal
	-1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	 1.0f,  1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
	-1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,
};

const uint32_t indicesControls[] = { 0, 1, 2, 2, 3, 0 };

/// Game object which serves as some kind of tutorial
class Controls : public GameObject
{
public:
	Controls(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader);
	~Controls();

	void Render(const Camera& cam, const std::vector<Light*>& lights, const State& state);
	void Update(float deltaTime);
	bool Init(const std::string& path, ResourceManager& manager);
	bool SolveCollisions(const glm::vec3 pos) const { return false; }

private:
	ShaderControls* m_shader; /// Shader used to render whole object
	float elapsedTime; /// Elapsed time from the start of the scene (modulo time of all frames)

	const float frameTime = 3.0f; /// Duration of a single frame
	const glm::vec2 pattern = glm::vec2(1.0f, 2.0f); /// Pattern of texture atlas
};

