//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    GameObject.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Partial implementation of game object
*/
//----------------------------------------------------------------------------------------

#include "Shader.h"
#include "GameObject.h"

GameObject::GameObject(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader)
	: m_position(position), m_scale(scale), m_rotation(rotation), m_shader(shader)
{
}


GameObject::~GameObject()
{
}

bool GameObject::Load(const std::string& path, ResourceManager& manager)
{
	try {
		m_submeshes = manager.getObject(path, *m_shader);
	}
	catch (LoadingFailedException) {
		return false;
	}

	return true;
}

GLuint GameObject::getTextureTarget(uint32_t number)
{
	switch (number)
	{
	case 0: return GL_TEXTURE0;
	case 1: return GL_TEXTURE1;
	case 2: return GL_TEXTURE2;
	case 3: return GL_TEXTURE3;
	case 4: return GL_TEXTURE4;
	default: return 0;
	}
}

void GameObject::renderObjectLights(const Camera& cam, ShaderLights* shader, const std::vector<Light*>& lights, const State& state)
{
	glUseProgram(shader->program);

	glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), m_position);
	modelMatrix = glm::rotate(modelMatrix, m_rotation.x, glm::vec3(1, 0, 0));
	modelMatrix = glm::rotate(modelMatrix, m_rotation.y, glm::vec3(0, 1, 0));
	modelMatrix = glm::rotate(modelMatrix, m_rotation.z, glm::vec3(0, 0, 1));
	modelMatrix = glm::scale(modelMatrix, m_scale);

	glm::mat4 PVM = cam.GetProjectionMatrix() * cam.GetViewMatrix() * modelMatrix;

	// Uniforms
	shader->AssignUniforms(PVM, cam.GetViewMatrix(), modelMatrix, lights, state);

	for (std::list<SubMesh>::iterator i = m_submeshes.begin(); i != m_submeshes.end(); ++i)
	{

		if (i->material.texture)
		{
			glUniform1i(shader->texLocation, 0);
			glActiveTexture(getTextureTarget(0));
			glBindTexture(GL_TEXTURE_2D, i->material.texture);

			glUniform1i(shader->useTextureLocation, 1);
		}
		else
		{
			glUniform1i(shader->useTextureLocation, 0);
		}

		// Material structure
		shader->AssignMaterial(i->material);

		glBindVertexArray(i->geometry.vertexArrayObject);
		glDrawElements(GL_TRIANGLES, i->geometry.numTriangles * 3, GL_UNSIGNED_INT, 0);
	}

	glBindVertexArray(0);
	glUseProgram(0);

	CHECK_GL_ERROR();
}