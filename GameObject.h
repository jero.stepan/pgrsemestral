//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    GameObject.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of game object - used to define most objects in the scene
*/
//----------------------------------------------------------------------------------------

#pragma once

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include "Camera.h"
#include "ShaderLights.h"
#include "Camera.h"
#include "ResourceManager.h"

/// Representation of most structures in the scene
class GameObject
{
public:
	GameObject(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader);
	~GameObject();

	/** Renders a object with given cam
	\param cam Camera to be viewed in
	\param lights Lights in the scene
	\param state Scene state
	*/
	virtual void Render(const Camera& cam, const std::vector<Light*>& lights, const State& state) = 0;
	/** Update method called each frame
	\param deltaTime Time from previous frame
	*/
	virtual void Update(float deltaTime) = 0;
	/** Initializing function called after contructor
	\param path Path to object resources
	\param manager Resource manager used to load .obj objects
	*/
	virtual bool Init(const std::string& path, ResourceManager& manager) = 0;
	/** Determines if object (camera) at given position collides with object.
	\param pos Position of other object
	*/
	virtual bool SolveCollisions(const glm::vec3 pos) const = 0;

	/** Sets a new position
	\param position New position
	*/
	void SetPosition(glm::vec3 position) { m_position = position; }
	/** Sets a new scale
	\param scale New scale
	*/
	void SetScale(glm::vec3 scale) { m_scale = scale; }
	/** Sets a new rotation
	\param rotation New rotation
	*/
	void SetRotation(glm::vec3 rotation) { m_rotation = rotation; }

private:
protected:
	
	std::list<SubMesh> m_submeshes; /// List of all submeshes
	Shader* m_shader; /// Shader used to render object

	glm::vec3 m_position; /// Position
	glm::vec3 m_scale; /// Scale
	glm::vec3 m_rotation; /// Rotation

	/** Loads .obj object from given path
	\param path Path to .obj file
	\param manager Resource manager used to load .obj objects
	*/
	bool Load(const std::string& path, ResourceManager& manager);
	/** Returns OpenGL texture target with given number
	\param number Number of the texture target
	\return Texture target
	*/
	static GLuint getTextureTarget(uint32_t number);
	/** Draws object with given shader
	\param cam Cam to be rendered in
	\param shader Shader to render with
	\param lights Scene lights
	\param state Scene state
	*/
	void renderObjectLights(const Camera& cam, ShaderLights* shader, const std::vector<Light*>& lights, const State& state);
};

