//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Glider.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of object glider and scripts for its movement
*/
//----------------------------------------------------------------------------------------

#include "Glider.h"

Glider::Glider(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader) : GameObject(position, scale, rotation, shader)
{
	m_shader = static_cast<ShaderLights*>(shader);
	m_elapsedTime = 0;
	m_startPosition = position;
	m_floating = false;
}


Glider::~Glider()
{
}

void Glider::Render(const Camera& cam, const std::vector<Light*>& lights, const State& state)
{
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glStencilFunc(GL_ALWAYS, id, 0xFF);
	
	glUseProgram(m_shader->program);

	glm::mat4 modelMatrix = (m_floating) ? alignOnCurve(glm::vec3(0, 1, 0)) : glm::translate(glm::mat4(1.0f), m_position);
	//if (m_floating) modelMatrix = glm::rotate(modelMatrix, glm::degrees(atan2(tmp.z, tmp.x)) / 2 + 90, glm::vec3(0, 0, 1));
	modelMatrix = glm::rotate(modelMatrix, 270.0f, glm::vec3(0, 1, 0));
	//modelMatrix = glm::rotate(modelMatrix, m_directionDif, glm::vec3(1, 0, 0));
	modelMatrix = glm::scale(modelMatrix, m_scale);

	glm::mat4 PVM = cam.GetProjectionMatrix() * cam.GetViewMatrix() * modelMatrix;

	// Uniforms
	m_shader->AssignUniforms(PVM, cam.GetViewMatrix(), modelMatrix, lights, state);

	for (std::list<SubMesh>::iterator i = m_submeshes.begin(); i != m_submeshes.end(); ++i)
	{

		if (i->material.texture)
		{
			glUniform1i(m_shader->texLocation, 0);
			glActiveTexture(getTextureTarget(0));
			glBindTexture(GL_TEXTURE_2D, i->material.texture);

			glUniform1i(m_shader->useTextureLocation, 1);
		}
		else
		{
			glUniform1i(m_shader->useTextureLocation, 0);
		}

		// Material structure
		m_shader->AssignMaterial(i->material);

		glBindVertexArray(i->geometry.vertexArrayObject);
		glDrawElements(GL_TRIANGLES, i->geometry.numTriangles * 3, GL_UNSIGNED_INT, 0);
	}

	glBindVertexArray(0);
	glUseProgram(0);

	CHECK_GL_ERROR();
	glDisable(GL_STENCIL_TEST);
}

void Glider::Update(float deltaTime)
{
	if (m_floating) {
		m_elapsedTime += deltaTime;

		float curveParamT = 0.8f * m_elapsedTime;

		m_position = m_towPosition + evaluateCurve(curvePoints, curvePointsNum, curveParamT);
		m_direction = derivateCurve(curvePoints, curvePointsNum, curveParamT);
		m_directionDif = glm::normalize(derivate2Curve(curvePoints, curvePointsNum, curveParamT));

		m_attachedCamera->SetPosition(m_position + glm::vec3(0, 5, 0));
	}

	//std::cout << atan2(m_directionDif.z, m_directionDif.x) << std::endl;
	//std::cout << m_directionDif.x << " " << m_directionDif.y << " " << m_directionDif.z << std::endl;
}

bool Glider::Init(const std::string& path, ResourceManager& manager)
{
	return GameObject::Load(path, manager);
}

glm::vec3 Glider::evaluateCurveSegment(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3&  p2, const glm::vec3&  p3, float t) const {
	glm::vec3 result;

	float t1 = t;
	float t2 = t*t;
	float t3 = t*t*t;

	result.x = 0.5f *(2 * p1.x + (-p0.x + p2.x) * t1 + (2 * p0.x - 5 * p1.x + 4 * p2.x - p3.x) * t2 + (-p0.x + 3 * p1.x - 3 * p2.x + p3.x) * t3);
	result.y = 0.5f *(2 * p1.y + (-p0.y + p2.y) * t1 + (2 * p0.y - 5 * p1.y + 4 * p2.y - p3.y) * t2 + (-p0.y + 3 * p1.y - 3 * p2.y + p3.y) * t3);
	result.z = 0.5f *(2 * p1.z + (-p0.z + p2.z) * t1 + (2 * p0.z - 5 * p1.z + 4 * p2.z - p3.z) * t2 + (-p0.z + 3 * p1.z - 3 * p2.z + p3.z) * t3);

	return result;
}

glm::vec3 Glider::derivateCurveSegment(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3&  p2, const glm::vec3&  p3, float t) const {
	glm::vec3 result;

	float t1 = 1.0f;
	float t2 = 2*t;
	float t3 = 3*t*t;

	result.x = 0.5f *((-p0.x + p2.x) * t1 + (2 * p0.x - 5 * p1.x + 4 * p2.x - p3.x) * t2 + (-p0.x + 3 * p1.x - 3 * p2.x + p3.x) * t3);
	result.y = 0.5f *((-p0.y + p2.y) * t1 + (2 * p0.y - 5 * p1.y + 4 * p2.y - p3.y) * t2 + (-p0.y + 3 * p1.y - 3 * p2.y + p3.y) * t3);
	result.z = 0.5f *((-p0.z + p2.z) * t1 + (2 * p0.z - 5 * p1.z + 4 * p2.z - p3.z) * t2 + (-p0.z + 3 * p1.z - 3 * p2.z + p3.z) * t3);

	return result;
}

glm::vec3 Glider::derivate2CurveSegment(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3&  p2, const glm::vec3&  p3, float t) const {
	glm::vec3 result;

	float t2 = 2.0f;
	float t3 = 6 * t;

	result.x = 0.5f *((2 * p0.x - 5 * p1.x + 4 * p2.x - p3.x) * t2 + (-p0.x + 3 * p1.x - 3 * p2.x + p3.x) * t3);
	result.y = 0.5f *((2 * p0.y - 5 * p1.y + 4 * p2.y - p3.y) * t2 + (-p0.y + 3 * p1.y - 3 * p2.y + p3.y) * t3);
	result.z = 0.5f *((2 * p0.z - 5 * p1.z + 4 * p2.z - p3.z) * t2 + (-p0.z + 3 * p1.z - 3 * p2.z + p3.z) * t3);

	return result;
}

glm::vec3 Glider::evaluateCurve(const glm::vec3 points[], uint32_t num, float t) const {
	glm::vec3 result(0.0, 0.0, 0.0);

	size_t i = (size_t)t;
	result = Glider::evaluateCurveSegment(points[(i + num - 1) % num], points[i % num], points[(i + 1) % num], points[(i + 2) % num], t - (float)i);

	return result;
}

glm::vec3 Glider::derivateCurve(const glm::vec3 points[], uint32_t num, float t) const {
	glm::vec3 result(1.0, 0.0, 0.0);

	size_t i = (size_t)t;
	result = Glider::derivateCurveSegment(points[(i + num - 1) % num], points[i % num], points[(i + 1) % num], points[(i + 2) % num], t - (float)i);

	return result;
}

glm::vec3 Glider::derivate2Curve(const glm::vec3 points[], uint32_t num, float t) const {
	glm::vec3 result(1.0, 0.0, 0.0);

	size_t i = (size_t)t;
	result = Glider::derivate2CurveSegment(points[(i + num - 1) % num], points[i % num], points[(i + 1) % num], points[(i + 2) % num], t - (float)i);

	return result;
}

glm::mat4 Glider::alignOnCurve(glm::vec3& up)
{
	glm::vec3 nz = glm::normalize(m_direction);			// Make new axis z
	glm::vec3 nx = glm::normalize(glm::cross(up, nz));	// Make new axis x
	glm::vec3 ny = glm::normalize(glm::cross(nz, nx));	// Make new axis y

	
	glm::mat4 transition = glm::mat4( // base transition matrix
		nx.x,			nx.y,			nx.z,			0.0f,
		ny.x,			ny.y,			ny.z,			0.0f,
		nz.x,			nz.y,			nz.z,			0.0f,
		m_position.x,	m_position.y,	m_position.z,	1.0f
	);

	return transition;
}

void Glider::Tow(Camera* cam)
{
	m_towPosition = glm::vec3(m_startPosition.x, 65, -65);

	m_elapsedTime = 0;
	m_floating = true;

	m_attachedCamera = cam;
}

void Glider::Land()
{
	m_floating = false;
	m_position = m_startPosition;
	m_attachedCamera->SetPosition(m_position + glm::vec3(0, 0, 10));
}