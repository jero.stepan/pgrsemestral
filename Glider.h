//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Glider.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of object glider
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "GameObject.h"

/// Control points for Catmull-Rom curve
const glm::vec3 curvePoints[] = {
	//glm::vec3( 0.0f, 0.0f,  0.0f),	// first point

	glm::vec3( 150.0f, 0.0f,  10.0f),	// beginning of first turn
	//glm::vec3( 170.0f, 0.0f,   0.0f),
	//glm::vec3( 200.0f, 0.0f, -10.0f),
	//glm::vec3( 170.0f, 0.0f, -20.0f),
	glm::vec3( 150.0f, 0.0f, -30.0f),  // end of first turn

	glm::vec3(-150.0f, 0.0f, -40.0f),  // beginning of second turn
	//glm::vec3(-170.0f, 0.0f, -20.0f),
	//glm::vec3(-200.0f, 0.0f,  0.0f),
	//glm::vec3(-170.0f, 0.0f,  5.0f),
	glm::vec3(-150.0f, 0.0f,  10.0f),  // end of second turn

};

/// Number of Catmull-Rom curve control points
const uint32_t curvePointsNum = sizeof(curvePoints) / sizeof(glm::vec3);

/// Game ogject glider
class Glider : public GameObject
{
public:
	Glider(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader);
	~Glider();

	void Render(const Camera& cam, const std::vector<Light*>& lights, const State& state);
	void Update(float deltaTime);
	bool Init(const std::string& path, ResourceManager& manager);
	bool SolveCollisions(const glm::vec3 pos) const { return false; }

	/** Tow the glider in the air and attach camera to it.
	\param cam Camera to be attached
	*/
	void Tow(Camera* cam);
	/** Land on the ground with the glider to start position.
	*/
	void Land();

private:
	ShaderLights* m_shader; /// Shader used for object rendering
	float m_elapsedTime; /// Time elapsed from scene beginning
	glm::vec3 m_startPosition; /// Position of glider at the beginning of the scene
	glm::vec3 m_towPosition; /// Position of glider when towed in the air
	glm::vec3 m_direction; /// Direction of the glider
	glm::vec3 m_directionDif; /// Direction difference 

	bool m_floating; /// Flag used forstating if a glider is in the air
	Camera* m_attachedCamera; /// Attached camera

	const uint32_t id = 1; /// ID used in stencil buffer - picking
	
	/** Evluates a position on Catmull-Rom curve segment given by control points.
	\param[in] p0 First control point
	\param[in] p1 Second control point
	\param[in] p2 Third control point
	\param[in] p3 Fourth control point
	\param[in] t Parameter within range <0,1>
	\return Position on the curve
	*/
	glm::vec3 evaluateCurveSegment(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3&  p2, const glm::vec3&  p3, float t) const;

	/** Evluates a first derivative on Catmull-Rom curve segment given by control points.
	\param[in] p0 First control point
	\param[in] p1 Second control point
	\param[in] p2 Third control point
	\param[in] p3 Fourth control point
	\param[in] t Parameter within range <0,1>
	\return First derivative of the curve
	*/
	glm::vec3 derivateCurveSegment(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3&  p2, const glm::vec3&  p3, float t) const;
	glm::vec3 derivate2CurveSegment(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3&  p2, const glm::vec3&  p3, float t) const;

	/** Evaluates a position on a closed Catmull-Rom curve
	\param[in] points Array of curve control points.
	\param[in] num Number of curve control points.
	\param[in] t Parameter for which the position shall be evaluated.
	\return Position on the curve for parameter t.
	*/
	glm::vec3 evaluateCurve(const glm::vec3 points[], uint32_t num, float t) const;

	/** Evaluates a first derivative of a closed Catmull-Rom curve
	\param[in] points Array of curve control points.
	\param[in] num Number of curve control points.
	\param[in] t Parameter for which the derivative shall be evaluated.
	\return First derivative of the curve for parameter t.
	*/
	glm::vec3 derivateCurve(const glm::vec3 points[], uint32_t num, float t) const;
	glm::vec3 derivate2Curve(const glm::vec3 points[], uint32_t num, float t) const;

	/** Aligns glider on Catmull-Rom curve
	\param up Up axis
	\return A model matrix shich consists of translation to correct position
	*/
	glm::mat4 alignOnCurve(glm::vec3& up);
};

