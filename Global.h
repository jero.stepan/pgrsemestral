//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Global.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definitions of structures used throughout the game objects
*/
//----------------------------------------------------------------------------------------

#pragma once

#include <pgr.h>
#include "Lights.h"

/// Geometry structure with pointers to OpenGL structures
struct MeshGeometry {
	GLuint vertexBufferObject; /// Pointer to OpenGL Vertex Buffer Object
	GLuint elementBufferObject; /// Pointer to OpenGL Element Buffer Object
	GLuint vertexArrayObject; /// Pointer to OpenGL Vertex Array Object
	uint32_t  numTriangles; /// Number of triangles of geometry
};

/// Material structure
struct Material {
	glm::vec3 diffuse; /// Diffuse color
	glm::vec3 ambient; /// Ambient
	glm::vec3 specular; /// Specular
	float shininess; /// Shininess
	GLuint texture; /// Pointer to OpenGL texture
};

/// Part of each GameObject
struct SubMesh {
	MeshGeometry geometry; /// Geometry structure
	Material material; /// Material
};

/// Struct for Exception
struct LoadingFailedException {
	
};

/// Scene bounds
struct Bounds {
	glm::vec3 min; /// Min point
	glm::vec3 max; /// Max point
};

/// Scene state
struct State {
	bool mouseLocked; /// Mouse lock flag, if false user can move around with mouse cursor
	bool floating; /// Glider flying flag, set when interacting with glider

	bool day; /// Daytime flag
	bool fog; /// Fog flag
};