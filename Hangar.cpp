//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Hangar.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of object Hangar
*/
//----------------------------------------------------------------------------------------

#include "hangar.h"

Hangar::Hangar(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader) : GameObject(position, scale, rotation, shader)
{
	m_shader = static_cast<ShaderLights*>(shader);
}


Hangar::~Hangar()
{
}

void Hangar::Render(const Camera& cam, const std::vector<Light*>& lights, const State& state)
{
	GameObject::renderObjectLights(cam, m_shader, lights, state);
}

void Hangar::Update(float deltaTime)
{

}

bool Hangar::Init(const std::string& path, ResourceManager& manager)
{
	return GameObject::Load(path, manager);
}