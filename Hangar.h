//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Hangar.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of object Hangar
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "GameObject.h"

/// Game object hangar
class Hangar : public GameObject
{
public:
	Hangar(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader);
	~Hangar();

	void Render(const Camera& cam, const std::vector<Light*>& lights, const State& state);
	void Update(float deltaTime);
	bool Init(const std::string& path, ResourceManager& manager);
	bool SolveCollisions(const glm::vec3 pos) const { return false; }

private:
	ShaderLights* m_shader; /// Shader used for object render
};

