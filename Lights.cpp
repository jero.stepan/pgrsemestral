//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Lights.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of scripts for lights
*/
//----------------------------------------------------------------------------------------

#include "Lights.h"

void LightDirectional::Update(float deltaTime)
{
	elapsedTime += deltaTime;
	if (elapsedTime < dayDuration / 2) { //Day
		float t = elapsedTime / (dayDuration / 2);
		float c = cos(PI * t - PI / 2);
		position = glm::vec3(c, sin(PI * t), 0) * distance;
		ambient = startAmbient * c;
		diffuse = startDiffuse * c;
		specular = startSpecular * c;
	}
	else if (elapsedTime < dayDuration) { // Night
		float t = (elapsedTime - dayDuration / 2) / (dayDuration / 2);
		float c = cos(PI * t - PI / 2);
		position = glm::vec3(c, sin(PI * t), 0) * distance;
		ambient = startAmbient * c * nightAttenuation;
		diffuse = startDiffuse * c * nightAttenuation;
		specular = startSpecular * c * nightAttenuation;
		
	}
	else {
		elapsedTime -= dayDuration;
	}
}

void LightSpot::Update(const glm::vec3& pos, const glm::vec3 dir)
{
	position = pos;
	spotDirection = dir;
}