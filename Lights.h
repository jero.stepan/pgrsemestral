//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Lights.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of structures for various light types
*/
//----------------------------------------------------------------------------------------

#pragma once

#include <pgr.h>

const uint32_t maxLights = 3; /// Number of lights in scene
const uint32_t dayDuration = 20; /// Duration of the day
const float nightAttenuation = 0.1f; /// Attenuation of the sun in the night
const float PI = 3.14159265359f; /// PI constant

/// Light structure
struct Light {
	glm::vec3 ambient; /// Ambient
	glm::vec3 diffuse; /// Diffuse
	glm::vec3 specular; /// Specular
	glm::vec3 position; /// Position 
	glm::vec3 spotDirection; /// Light spot direction
	glm::vec3 attenuation; /// Attenuation
	float spotCosCutOff; /// Cos of cutoff angle
	float spotExponent; /// Spot exponent

	Light(glm::vec3 amb, glm::vec3 diff, glm::vec3 spec, glm::vec3 pos, glm::vec3 dir, glm::vec3 att, float cutOff, float exp) :
		ambient(amb), diffuse(diff), specular(spec), position(pos), spotDirection(dir), attenuation(att), spotCosCutOff(cutOff), spotExponent(exp) {}
};

/// Directional light structure
struct LightDirectional : public Light {
	LightDirectional(glm::vec3 amb, glm::vec3 diff, glm::vec3 spec, glm::vec3 pos, glm::vec3 dir, glm::vec3 att, float cutOff, float exp) :
		Light(amb, diff, spec, pos, dir, att, cutOff, exp), elapsedTime(0), distance(glm::length(pos)), startAmbient(amb), startDiffuse(diff), startSpecular(spec){}

	/** Update function used to execute scripts in each frame
	\param deltaTime Time from last update.
	*/
	void Update(float deltaTime);

	float elapsedTime; /// Elapsed time from the beginning of the scene
	float distance; /// Distance from world origin
	glm::vec3 startAmbient; /// Ambient component of the light at the beginning of the scene
	glm::vec3 startDiffuse; /// Diffuse component of the light at the beginning of the scene
	glm::vec3 startSpecular; /// Specular component of the light at the beginning of the scene
};

struct LightSpot : public Light {
	LightSpot(glm::vec3 amb, glm::vec3 diff, glm::vec3 spec, glm::vec3 pos, glm::vec3 dir, glm::vec3 att, float cutOff, float exp) :
		Light(amb, diff, spec, pos, dir, att, cutOff, exp) {}

	/** Update function used to execute scripts in each frame
	\param deltaTime Time from last update.
	*/
	void Update(const glm::vec3& pos, const glm::vec3 dir);
};