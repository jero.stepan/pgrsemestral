//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ObjectLoader.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of object loader (supports only wavefront format)
*/
//----------------------------------------------------------------------------------------

#include "ObjectLoader.h"

#include <fstream>
#include <vector>
#include <sstream>
#include <iostream>

using namespace std;

ObjectLoader::ObjectLoader()
{
}


ObjectLoader::~ObjectLoader()
{
	for (list<LoaderMesh>::iterator i = m_meshes.begin(); i != m_meshes.end(); ++i)
	{
		delete[] i->m_indices;
		delete[] i->m_positions;
		delete[] i->m_normals;
		delete[] i->m_UVs;

	}
}

bool ObjectLoader::Load(const std::string& path)
{
	FILE* inputFile = fopen(path.c_str(), "r");

	if (!inputFile) {
		cerr << "Could not open: " << path << endl;
		return false;
	}

	const uint32_t bufferSize = 2048;
	char buffer[bufferSize];
	char token[256];
	
	vector<glm::vec3> tmpPositions;
	vector<glm::vec3> tmpNormals;
	vector<glm::vec2> tmpUVs;
	vector<uint32_t> indices;
	map<VertexIndices, uint32_t> vertexMap;
	LoaderMesh mesh;
	uint32_t offsetPositions = 0, offsetNormals = 0, offsetUVs = 0;
	map<string, Material> materials;
	float scale = 1;

 	bool matSet = false;
	char c;
	uint32_t index;

	while (!feof(inputFile))
	{
		index = 0;
		while (fread(&c, sizeof(char), 1, inputFile) && c != ' ' && c != '\n') { // Parse token
			token[index] = c;
			++index;
		}
		token[index] = 0;

		if (!strcmp(token, "v")) { // Found vertex position
			glm::vec3 position;
			if (fscanf(inputFile, "%f %f %f", &position.x, &position.y, &position.z) != 3) {
				cerr << "Failed reading vertex position in: " << path << endl;
				return false;
			}
			scale = max(scale, fabsf(position.x));
			scale = max(scale, fabsf(position.y));
			scale = max(scale, fabsf(position.z));
			tmpPositions.push_back(position);
		}
		else if (!strcmp(token, "vn")) { // Found normal
			glm::vec3 normal;
			if (fscanf(inputFile, "%f %f %f", &normal.x, &normal.y, &normal.z) != 3) {
				cerr << "Failed reading normal in: " << path << endl;
				return false;
			}
			tmpNormals.push_back(normal);
		}
		else if (!strcmp(token, "vt")) { // Found UV
			glm::vec2 uv;
			if (fscanf(inputFile, "%f %f", &uv.x, &uv.y) != 2) {
				cerr << "Failed reading UV in: " << path << endl;
				return false;
			}
			tmpUVs.push_back(uv);
		}
		else if (!strcmp(token, "f")) { // Found face
									 //TODO:: support not only triangulated faces?
			Face face;
			for (uint32_t i = 0; i < face.numVertices; ++i) {
				face.indices[i] = parseVertexIndices(inputFile);
				face.indices[i].m_pIndex -= offsetPositions;
				face.indices[i].m_nIndex -= offsetNormals;
				face.indices[i].m_tIndex -= offsetUVs;

				map<VertexIndices, uint32_t>::iterator iter = vertexMap.find(face.indices[i]);
				if (iter == vertexMap.end()) { // This combination of position, normal and UV was not found
					iter = vertexMap.insert(make_pair(face.indices[i], vertexMap.size())).first; // Create a new one and assign it a new index
				}
				indices.push_back(iter->second); // Combination found (or just created), add existing index to indices array
			}
		}
		else if (!strcmp(token, "mtllib"))
		{
			if (matSet) continue;
			
			if (fscanf(inputFile, "%2000s", buffer) != 1) {
				cerr << "Failed to read material path." << endl;
				return false;
			}
			string materialPath = path.substr(0, path.find_last_of("/")) + "/" + buffer;
			if (!loadMaterials(materialPath, materials)) return false;
		}
		else if (!strcmp(token, "o")) // Next object
		{
			if (!indices.size()) continue; // We haven't really read anything by now - first object
			mesh.m_numTriangles = indices.size()/3;
			offsetPositions += tmpPositions.size();
			offsetNormals += tmpNormals.size();
			offsetUVs += tmpUVs.size();
			finalizeMesh(mesh, tmpPositions, tmpNormals, tmpUVs, indices, vertexMap, scale);
		}
		else if (!strcmp(token, "usemtl")) {
			if (fscanf(inputFile, "%2048s", buffer) != 1) {
				cerr << "Failed to read material name." << endl;
				return false;
			}
			string materialName(buffer);
			
			map<string, Material>::iterator iter = materials.find(materialName);
			if (iter == materials.end()) {
				cerr << "Could not found material: " << materialName << " in: " << path << endl;
				return false;
			}
			mesh.m_material = iter->second;
		}
	}

	mesh.m_numTriangles = indices.size() / 3;
	finalizeMesh(mesh, tmpPositions, tmpNormals, tmpUVs, indices, vertexMap, scale);

	fclose(inputFile);

	return true;
}

ObjectLoader::VertexIndices ObjectLoader::parseVertexIndices(FILE* file)
{
	VertexIndices indices;
	char d1, d2;

	if (fscanf(file, "%u%c%u%c%u", &indices.m_pIndex, &d1, &indices.m_tIndex, &d2, &indices.m_nIndex) != 5) {
		cerr << "Failed to parse face indices." << endl;
		throw LoadingFailedException();
	}

	/*
	fscanf(file, "%u%d", &indices.m_pIndex, &d);
	d = fgetc(file);
	if (d != '/') {
		ungetc(d, file);
		fscanf(file, "%u%d", &indices.m_pIndex, &d);
	}*/

	indices.CountHash();

	return indices;
}

bool ObjectLoader::loadMaterials(const string& path, map<string, Material>& materials)
{
	FILE* inputFile = fopen(path.c_str(), "r");

	if (!inputFile) {
		cerr << "Could not open: " << path << endl;
		return false;
	}

	string line;
	Material material;
	char name[1024];

	char token[256], c;
	size_t index;

	while (!feof(inputFile))
	{
		index = 0;
		while (fread(&c, sizeof(char), 1, inputFile) && c != ' ' && c != '\n') {
			token[index] = c;
			++index;
		}
		token[index] = 0;

		if (!strcmp(token, "Ns")) {
			if (fscanf(inputFile, "%f", &material.shininess) != 1) {
				cerr << "Failed reading material shininess in: " << path << endl;
				return false;
			}
		}
		else if (!strcmp(token, "Ka")) {
			if (fscanf(inputFile, "%f%f%f", &material.ambient.x, &material.ambient.y, &material.ambient.z) != 3) {
				cerr << "Failed reading material ambient in: " << path << endl;
				return false;
			}
		}
		else if (!strcmp(token, "Kd")) {
			if (fscanf(inputFile, "%f%f%f", &material.diffuse.x, &material.diffuse.y, &material.diffuse.z) != 3) {
				cerr << "Failed reading material diffuse in: " << path << endl;
				return false;
			}
		}
		else if (!strcmp(token, "Ks")) {
			if (fscanf(inputFile, "%f%f%f", &material.specular.x, &material.specular.y, &material.specular.z) != 3) {
				cerr << "Failed reading material specular in: " << path << endl;
				return false;
			}
		}
		else if (!strcmp(token, "map_Kd")) {
			char filename[1024];
			if (fscanf(inputFile, "%1023s", filename) != 1) {
				cerr << "Failed to load texture path." << endl;
				return false;
			}
			string texturePath = path.substr(0, path.find_last_of("/")) + "/" + filename;

			std::cout << "Loading texture file: " << texturePath << std::endl;
			GLuint texture = pgr::createTexture(texturePath);

			if (!texture)
			{
				std::cerr << "Error loading texture: " << texturePath << std::endl;
				return false;
			}
			material.texture = texture;
			materials.insert(make_pair(name, material));
		}
		else if (!strcmp(token, "newmtl")) {
			if (fscanf(inputFile, "%1023s", name) != 1) {
				cerr << "Failed to load material name." << endl;
				return false;
			}
		}
	}

	fclose(inputFile);

	return true;
}

void ObjectLoader::VertexIndices::CountHash()
{
	m_hash = base;
	m_hash = multiplier * m_hash + m_pIndex;
	m_hash = multiplier * m_hash + m_nIndex;
	m_hash = multiplier * m_hash + m_tIndex;
}

bool ObjectLoader::VertexIndices::operator<(const VertexIndices& indices) const
{
	return m_hash < indices.m_hash;
}

const ObjectLoader::VertexIndices& ObjectLoader::VertexIndices::operator=(const VertexIndices& indices)
{
	m_pIndex = indices.m_pIndex;
	m_nIndex = indices.m_nIndex;
	m_tIndex = indices.m_tIndex;
	CountHash();

	return *this;
}

bool ObjectLoader::VertexIndices::operator==(const VertexIndices& indices) const
{
	return m_hash == indices.m_hash;
}

void ObjectLoader::finalizeMesh(LoaderMesh& mesh, vector<glm::vec3>& tmpPositions, vector<glm::vec3>& tmpNormals, vector<glm::vec2>& tmpUVs, vector<uint32_t>& indices, map<VertexIndices, uint32_t>& vertexMap, float scale)
{
	mesh.m_numIndices = indices.size();
	mesh.m_indices = new uint32_t[mesh.m_numIndices];
	memcpy(mesh.m_indices, &indices[0], mesh.m_numIndices * sizeof(uint32_t));

	mesh.m_numPositions = vertexMap.size() * 3;
	mesh.m_positions = new float[mesh.m_numPositions];
	mesh.m_numNormals = vertexMap.size() * 3;
	mesh.m_normals = new float[mesh.m_numNormals];
	mesh.m_numUVs = vertexMap.size() * 2;
	mesh.m_UVs = new float[mesh.m_numUVs];

	for (map<VertexIndices, uint32_t>::iterator i = vertexMap.begin(); i != vertexMap.end(); ++i)
	{
		mesh.m_positions[i->second * 3 + 0] = tmpPositions[i->first.m_pIndex - 1].x / scale; // *3 - for one index we have 3 coordinates - xyz
		mesh.m_positions[i->second * 3 + 1] = tmpPositions[i->first.m_pIndex - 1].y / scale; // -1: We have loaded indexed beginning with 1, but C++ arrays have indexes beginning with 0
		mesh.m_positions[i->second * 3 + 2] = tmpPositions[i->first.m_pIndex - 1].z / scale;

		mesh.m_normals[i->second * 3 + 0] = tmpNormals[i->first.m_nIndex - 1].x;
		mesh.m_normals[i->second * 3 + 1] = tmpNormals[i->first.m_nIndex - 1].y;
		mesh.m_normals[i->second * 3 + 2] = tmpNormals[i->first.m_nIndex - 1].z;

		mesh.m_UVs[i->second * 2 + 0] = tmpUVs[i->first.m_tIndex - 1].x;
		mesh.m_UVs[i->second * 2 + 1] = tmpUVs[i->first.m_tIndex - 1].y;
	}

	m_meshes.push_back(mesh);

	tmpPositions.clear();
	tmpNormals.clear();
	tmpUVs.clear();
	indices.clear();
	vertexMap.clear();
}