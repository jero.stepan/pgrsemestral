//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ObjectLoader.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of object loader (supports only wavefront format) and all depending structures
*/
//----------------------------------------------------------------------------------------

#pragma once

#include <cstdint>
#include <list>
#include <string>
#include <map>
#include "pgr.h"
#include "Global.h"

/// LoaderMesh is used to load data using OpenGL
struct LoaderMesh {
	float* m_positions; /// Array of vertex positions
	float* m_normals; /// Array of vertex normals
	float* m_UVs; /// Array of vertex UVs
	uint32_t* m_indices; /// Array of elements indices

	Material m_material; /// Mesh material structure

	uint32_t m_numTriangles; /// Number of triangles
	uint32_t m_numPositions; /// Size of positions array
	uint32_t m_numNormals; /// Size of normals array
	uint32_t m_numUVs; /// Size of UVs array
	uint32_t m_numIndices; /// Size of indices array
};

/// ObjectLoader is used to load object and its materials
class ObjectLoader
{
public:
	ObjectLoader();
	~ObjectLoader();

	/** Loads meshes to structure LoaderMesh
	\param path Path to the object file
	\return True on success, otherwise false
	*/
	bool Load(const std::string& path);
	/** Returns all lopaded meshes
	\return List of LoaderMeshes
	*/
	const std::list<LoaderMesh>& GetMeshes() const { return m_meshes; }

private:

	/// Structure used for storing indices to arrays
	struct VertexIndices {
		uint32_t m_pIndex; /// Position index
		uint32_t m_nIndex; /// Normal index
		uint32_t m_tIndex; /// UV index

		const uint64_t base = 37; /// Hash base
		const uint64_t multiplier = 123; /// Hash multiplier

		uint64_t m_hash; /// Hash used for comparison between structures

		/// Counts hash from indices using base and multiplier
		void CountHash();
		/** Comparison operator - comparing hashes.
		\param indices Another structure to compare with
		\return True if less, otherwise false
		*/
		bool operator<(const VertexIndices& indices) const;
		/** Assignment operator.
		\param indices Another structure to assign.
		\return Reference to this instance
		*/
		const VertexIndices& operator=(const VertexIndices& indices);
		/** Comparison operator.
		\param indices Another structure to compare with
		\return True when equal, otherwise false
		*/
		bool operator==(const VertexIndices& indices) const;
	};

	/// Structure used for loading face
	struct Face {
		VertexIndices indices[3]; /// Vertex indices
		uint32_t numVertices = 3; /// Number of vertices in face
	};

	std::list<LoaderMesh> m_meshes; /// List of LoaderMeshes

	/** Parses single face vertex indices (position, UV, normal index) from given file stream.
	\param file File to read from
	\return Parsed vertex indices
	*/
	VertexIndices parseVertexIndices(FILE* file);
	/** Loads materials from file in given path
	\param path Path to material library
	\param materials Map of material name and material structure
	\return True on success, otherwise false
	*/
	bool loadMaterials(const std::string& path, std::map<std::string, Material>& materials);
	/** Fills the LoaderMesh structure with positions, normals, UVs, materials, etc. by VertexIndices.
	\param mesh A mesh to be filled
	\param tmpPositions Array with loaded positions from obj file
	\param tmpNormals Array with loaded normals from obj file
	\param tmpUVs Array with loaded UVs from obj file
	\param indices Array with final vertex indices constructed from face indices
	\param vertexMap Array with vertex index and index to final array
	\param scale Scale of object to match <-1, 1>^3
	*/
	void finalizeMesh(LoaderMesh& mesh, std::vector<glm::vec3>& tmpPositions, std::vector<glm::vec3>& tmpNormals, 
		std::vector<glm::vec2>& tmpUVs, std::vector<uint32_t>& indices, std::map<VertexIndices, uint32_t>& vertexMap, float scale);
};

