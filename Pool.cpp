//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Pool.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of object Pool and script for moving water texture
*/
//----------------------------------------------------------------------------------------

#include "Pool.h"

Pool::Pool(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader) : GameObject(position, scale, rotation, shader)
{
	m_shader = static_cast<ShaderPool*>(shader);
	m_movementDir = -1;
}


Pool::~Pool()
{
}

void Pool::Render(const Camera& cam, const std::vector<Light*>& lights, const State& state)
{
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glStencilFunc(GL_ALWAYS, id, 0xFF);

	glUseProgram(m_shader->program);

	glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), m_position);
	modelMatrix = glm::rotate(modelMatrix, m_rotation.x, glm::vec3(1, 0, 0));
	modelMatrix = glm::rotate(modelMatrix, m_rotation.y, glm::vec3(0, 1, 0));
	modelMatrix = glm::rotate(modelMatrix, m_rotation.z, glm::vec3(0, 0, 1));
	modelMatrix = glm::scale(modelMatrix, m_scale);

	glm::mat4 PVM = cam.GetProjectionMatrix() * cam.GetViewMatrix() * modelMatrix;
	m_shader->AssignUniforms(PVM, cam.GetViewMatrix(), modelMatrix, lights, state);
	glUniform1f(m_shader->elapsedTimeLocation, m_elapsed);

	glm::mat2 movementMatrix;
	
	for (std::list<SubMesh>::iterator i = m_submeshes.begin(); i != m_submeshes.end(); ++i) {

		if (i != m_submeshes.begin()) {
			movementMatrix = glm::mat2(glm::vec2(0.0f, 0.0f), glm::vec2(0.0f, 0.0f));
		}
		else {
			movementMatrix = glm::mat2(glm::vec2(0.0f, m_movementDir * -1.0f), glm::vec2(0.0f, m_movementDir * 2.0f));
		}
		glUniformMatrix2fv(m_shader->movementLocation, 1, GL_FALSE, glm::value_ptr(movementMatrix));

		if (i->material.texture) {
			glUniform1i(m_shader->texLocation, 0);
			glActiveTexture(getTextureTarget(0));
			glBindTexture(GL_TEXTURE_2D, i->material.texture);
		}

		// Material structure
		m_shader->AssignMaterial(i->material);

		glBindVertexArray(i->geometry.vertexArrayObject);
		glDrawElements(GL_TRIANGLES, i->geometry.numTriangles * 3, GL_UNSIGNED_INT, 0);
	}

	glBindVertexArray(0);
	glUseProgram(0);

	CHECK_GL_ERROR();
	glDisable(GL_STENCIL_TEST);
}

void Pool::Update(float deltaTime)
{
	m_elapsed += deltaTime / 20;
}

bool Pool::Init(const std::string& path, ResourceManager& manager)
{
	if (!GameObject::Load(path, manager)) return false;
	m_elapsed = 0;
	return true;
}

void Pool::Interact()
{
	m_movementDir *= -1;
}