//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Pool.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of object Pool
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "GameObject.h"
#include "ShaderPool.h"

/// Game object pool
class Pool : public GameObject
{
public:
	Pool(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader);
	~Pool();
	void Render(const Camera& cam, const std::vector<Light*>& lights, const State& state);
	void Update(float deltaTime);
	bool Init(const std::string& path, ResourceManager& manager);
	bool SolveCollisions(const glm::vec3 pos) const { return false; }

	/// Make some interaction after picking
	void Interact();

private:
	ShaderPool* m_shader; /// Shader used for rendering the object
	float m_elapsed; /// Time elapsed from scene beginning
	int32_t m_movementDir; /// Direction of water movement

	const uint32_t id = 3; /// ID used in stencil buffer - picking
};

