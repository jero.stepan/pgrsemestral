//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Program.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of main class - Program. It handles all the inputs and callbacks from GLUT, then it calls update functions of game objects and lastly it handles rendering.
*/
//----------------------------------------------------------------------------------------

#include "Program.h"

Program::Program()
{
}

Program::~Program()
{
	destroy();
}

bool Program::Init(int32_t width, int32_t height)
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	std::ifstream inputFile(configPath);

	if (!inputFile.is_open())
	{
		std::cerr << "Could not open: " << configPath << std::endl;
		return false;
	}

	destroy();
	m_cameras.clear();
	m_shaders.clear();
	m_lights.clear();
	m_objects.clear();

	m_width = width;
	m_height = height;

	if (!loadCameras(inputFile)) return false;
	if (!loadShaders(inputFile)) return false;
	if (!loadLights(inputFile)) return false;
	if (!loadGameObjects(inputFile)) return false;

	m_lastTime = glutGet(GLUT_ELAPSED_TIME);
	
	m_state.mouseLocked = true;
	m_state.floating = false;

	m_state.day = true;
	m_state.fog = false;
	
	m_bounds.max = glm::vec3(300, 0, 300);
	m_bounds.min = glm::vec3(-300, 0, -20);

	inputFile.close();

	return true;
}

void Program::loop()
{
	processInput(); // Get input

	for (std::list<GameObject*>::iterator i = m_objects.begin(); i != m_objects.end(); ++i) (*i)->Update(m_time.timeS); // Update objects

	m_sun->Update(m_time.timeS);
	m_torchlight->Update(m_actualCam->GetPosition(), m_actualCam->GetDirection());
}

bool Program::loadCameras(std::ifstream& stream)
{
	std::string token;
	glm::vec3 position, upVector;
	int staticCam;
	float horAngle, vertAngle;

	if (!(stream >> token && token == "Cameras")) return false;

	while ((stream >> token) && token != "-") {
		if (token == "position") stream >> position.x >> position.y >> position.z;
		else return false;
		if (stream >> token && token == "upvector") stream >> upVector.x >> upVector.y >> upVector.z;
		else return false;
		if (stream >> token && token == "horAngle") stream >> horAngle;
		else return false;
		if (stream >> token && token == "vertAngle") stream >> vertAngle;
		else return false;
		if (stream >> token && token == "static") stream >> staticCam;
		else return false;

		m_cameras.push_back(Camera(m_width, m_height, position, upVector, vertAngle, horAngle));
		if (staticCam) m_cameras.back().ToggleStatic();
	}

	m_actualCam = m_cameras.begin();
	return true;
}

bool Program::loadShaders(std::ifstream& stream)
{
	std::string token, type, path;
	std::pair<std::map<std::string, Shader*>::iterator, bool> iter;

	if (!(stream >> token && token == "Shaders")) return false;

	while ((stream >> token) && token != "-")
	{
		if (token == "type") stream >> type;
		else return false;
		if (stream >> token && token == "path") stream >> path;
		else return false;

		if (type == "lights") iter = m_shaders.insert(make_pair(type, new ShaderLights()));
		else if (type == "pool") iter = m_shaders.insert(make_pair(type, new ShaderPool()));
		else if (type == "skybox") iter = m_shaders.insert(make_pair(type, new ShaderSkybox()));
		else if (type == "controls") iter = m_shaders.insert(make_pair(type, new ShaderControls()));

		if (!(iter.second)) {
			std::cerr << "Error inserting new shader into container." << std::endl;
			return false;
		}
		if (!iter.first->second->Load(path)) {
			return false;
		}
		iter.first->second->Init();
	}
	return true;
}

bool Program::loadLights(std::ifstream& stream)
{
	std::string token;
	glm::vec3 amb, diff, spec, pos, dir, att;
	float cutOff, exp;

	if (!(stream >> token && token == "Lights")) return false;

	while (stream >> token && token != "-")
	{
		if (token == "ambient") stream >> amb.x >> amb.y >> amb.z;
		else return false;
		if (stream >> token && token == "diffuse") stream >> diff.x >> diff.y >> diff.z;
		else return false;
		if (stream >> token && token == "specular") stream >> spec.x >> spec.y >> spec.z;
		else return false;
		if (stream >> token && token == "position") stream >> pos.x >> pos.y >> pos.z;
		else return false;
		if (stream >> token && token == "direction") stream >> dir.x >> dir.y >> dir.z;
		else return false;
		if (stream >> token && token == "attenuation") stream >> att.x >> att.y >> att.z;
		else return false;
		if (stream >> token && token == "cutoff") stream >> cutOff;
		else return false;
		if (stream >> token && token == "spotexponent") stream >> exp;
		else return false;

		if (!att.x && !att.y && !att.z) {
			m_sun = new LightDirectional(amb, diff, spec, pos, dir, att, cutOff, exp);
			m_lights.push_back(m_sun);
		}
		else if (cutOff) {
			m_torchlight = new LightSpot(amb, diff, spec, pos, dir, att, cutOff, exp);
			m_lights.push_back(m_torchlight);
		}
		else {
			m_lights.push_back(new Light(amb, diff, spec, pos, dir, att, cutOff, exp));
		}
	}

	return true;
}

bool Program::loadGameObjects(std::ifstream& stream)
{
	glm::vec3 position, scale, rotation;
	std::string objectPath, shaderName, objectType;
	std::string token;
	GameObject* object;

	if (!(stream >> token && token == "Objects")) return false;

	while (stream >> token && token != "-")
	{
		if (token == "type") stream >> objectType;
		else return false;
		if (stream >> token && token == "position") stream >> position.x >> position.y >> position.z;
		else return false;
		if (stream >> token && token == "scale") stream >> scale.x >> scale.y >> scale.z;
		else return false;
		if (stream >> token && token == "rotation") stream >> rotation.x >> rotation.y >> rotation.z;
		else return false;
		if (stream >> token && token == "shader") stream >> shaderName;
		else return false;
		if (stream >> token && token == "path") stream >> objectPath;
		else return false;

		std::map<std::string, Shader*>::iterator iter = m_shaders.find(shaderName);
		if (iter == m_shaders.end())
		{
			std::cerr << "Could not find shader: " << shaderName << std::endl;
			return false;
		}

		if (objectType == "Terrain")  object = new Terrain(position, scale, rotation, iter->second);
		else if (objectType == "Hangar") object = new Hangar(position, scale, rotation, iter->second);
		else if (objectType == "Windsock") {
			m_windsock = new Windsock(position, scale, rotation, iter->second);
			object = m_windsock;
		}
		else if (objectType == "Pool") {
			m_pool = new Pool(position, scale, rotation, iter->second);
			object = m_pool;
		}
		else if (objectType == "Tree") object = new Tree(position, scale, rotation, iter->second);
		else if (objectType == "Skybox") object = new Skybox(position, scale, rotation, iter->second);
		else if (objectType == "Controls") object = new Controls(position, scale, rotation, iter->second);
		else if (objectType == "Glider") {
			m_glider = new Glider(position, scale, rotation, iter->second);
			object = m_glider;
		}
		
		if (!object->Init(objectPath, m_resManager)) return false;

		m_objects.push_back(object);
	}

	return true;
}

void Program::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	for (std::list<GameObject*>::iterator i = m_objects.begin(); i != m_objects.end(); ++i) (*i)->Render(*m_actualCam, m_lights, m_state);
	
	CHECK_GL_ERROR();
	glutSwapBuffers();
}

void Program::OnReshapeEvent(int32_t width, int32_t height)
{
	glViewport(0, 0, width, height);
	for (std::list<Camera>::iterator i = m_cameras.begin(); i != m_cameras.end(); ++i) i->SetSize(width, height);
	m_width = width;
	m_height = height;
}

void Program::OnKeyEvent(uint8_t key)
{
	switch (key) {
	case 27:
		if (m_state.floating) {
			m_state.floating = false;
			if (m_glider) m_glider->Land();
			else std::cerr << "Glider pointer has NULL value!" << std::endl;
			break;
		}
		glutLeaveMainLoop();
		break;
	case 'c':
		cycleCameras();
		break;
	case ' ':
		m_actualCam->ToggleStatic();
		break;
	case 'r':
		Init(m_width, m_height);
		OnReshapeEvent(m_width, m_height);
		break;
	case 'l':
		m_state.mouseLocked = !m_state.mouseLocked;
		break;
	}
}

void Program::OnMouseEvent(int32_t buttonPressed, int32_t buttonState, int32_t mouseX, int32_t mouseY)
{
	if ((buttonPressed == GLUT_LEFT_BUTTON) && (buttonState == GLUT_DOWN)) {
		unsigned char objectID = 0;
		glReadPixels(mouseX, m_height - mouseY - 1, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &objectID);

		switch (objectID) {
		case 1:
			if (m_glider) {
				m_glider->Tow(&(*m_actualCam));
				m_state.floating = true;
			}
			else std::cerr << "Glider pointer has NULL value!" << std::endl;
			break;
		case 2:
			if (m_windsock) m_windsock->Interact();
			else std::cerr << "Windsock pointer has NULL value!" << std::endl;
			break;
		case 3:
			if (m_pool) m_pool->Interact();
			else std::cerr << "Pool pointer has NULL value!" << std::endl;
			break;
		default:
			std::cout << "Unrecognised mouse event." << std::endl;
		}
	}
}

void Program::OnMousePassiveEvent(int32_t mouseX, int32_t mouseY)
{
	if (!m_state.mouseLocked) return;
	if (mouseY != m_height / 2 || mouseX != m_width / 2) {
		m_actualCam->Rotate(mouseX, mouseY, m_time.timeS);
		glutWarpPointer(m_width / 2, m_height / 2);
	}
}

void Program::OnTimerEvent()
{
	int32_t start = glutGet(GLUT_ELAPSED_TIME);
	m_time.timeMs = start - m_lastTime;
	m_time.timeS = m_time.timeMs * 0.001f;

	loop();
	m_lastTime = glutGet(GLUT_ELAPSED_TIME);
}

bool Program::SolveCollisions(const glm::vec3& pos) const
{
	if (pos.x < m_bounds.min.x || pos.x > m_bounds.max.x || pos.z < m_bounds.min.z || pos.z > m_bounds.max.z) return true;

	for (std::list<GameObject*>::const_iterator i = m_objects.cbegin(); i != m_objects.cend(); ++i) {
		if ((*i)->SolveCollisions(pos)) return true;
	}

	return false;
}

void Program::cycleCameras()
{
	if (++m_actualCam == m_cameras.end()) m_actualCam = m_cameras.begin();
}

void Program::processInput()
{
	if (m_keyMap[K_UP]) m_actualCam->Move(30.0f, m_time.timeS);
	if (m_keyMap[K_DOWN]) m_actualCam->Move(-30.0f, m_time.timeS);
	if (m_keyMap[K_LEFT]) m_actualCam->MoveAside(20.0f, m_time.timeS);
	if (m_keyMap[K_RIGHT]) m_actualCam->MoveAside(-20.0f, m_time.timeS);
}

void Program::destroy()
{
	for (std::list<GameObject*>::iterator i = m_objects.begin(); i != m_objects.end(); ++i)
	{
		delete *i;
	}
	for (std::map<std::string, Shader*>::iterator i = m_shaders.begin(); i != m_shaders.end(); ++i)
	{
		delete i->second;
	}
	for (std::vector<Light*>::iterator i = m_lights.begin(); i != m_lights.end(); ++i)
	{
		delete *i;
	}
}