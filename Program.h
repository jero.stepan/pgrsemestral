//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Program.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of main class - Program. It handles all the inputs and callbacks from GLUT, then it calls update functions of game objects and lastly it handles rendering.
*/
//----------------------------------------------------------------------------------------

#pragma once

#ifndef __PROGRAM_H
#define __PROGRAM_H

#include <list>
#include <fstream>

#include "GameObject.h"
#include "ShaderLights.h"
#include "ShaderPool.h"
#include "ShaderSkybox.h"
#include "ShaderControls.h"
#include "ResourceManager.h"

#include "Terrain.h"
#include "Pool.h"
#include "Hangar.h"
#include "Windsock.h"
#include "Glider.h"
#include "Tree.h"
#include "Skybox.h"
#include "Controls.h"

/// Map for special keys
enum KeyMap { K_UP = 0, K_DOWN = 1, K_LEFT = 2, K_RIGHT = 3 };

/// Structure for soting time variables
struct Time {
	int32_t timeMs; /// Delta time in miliseconds
	float timeS; /// Delta time in seconds
};

/// Main structure containing all objects, lights, etc. Handles all inputs and outputs.
class Program
{
public:
	Program();
	~Program();

	/** Init whole program
	\param width Window width
	\param height Window height
	\return True on success, otherwise false
	*/
	bool Init(int32_t width, int32_t height);
	/** Renders whole scene
	*/
	void Render();
	/** Reshape event handler
	\param width New window width
	\param height New window height
	*/
	void OnReshapeEvent(int32_t width, int32_t height);
	/** Keyboard event handler
	\param key Pressed key
	*/
	void OnKeyEvent(uint8_t key);
	/** Mouse button event handler
	\param buttonPressed Identifier of pressed button
	\param buttonState State of pressed button
	\param mouseX Position on X axis
	\param mouseY Position on Y axis
	*/
	void OnMouseEvent(int32_t buttonPressed, int32_t buttonState, int32_t mouseX, int32_t mouseY);
	/** Mouse motion event handler
	\param mouseX Position on X axis
	\param mouseY Position on Y axis
	*/
	void OnMousePassiveEvent(int32_t mouseX, int32_t mouseY);
	/** Timer event handler
	*/
	void OnTimerEvent();

	/// \return Keyboard map
	bool* GetKeyMap() { return m_keyMap; }
	/// \return Scene state
	State& GetState() { return m_state; }
	/// Sets first camera in array as an actual camera
	void SetFirstCamera() { m_actualCam = m_cameras.begin(); }
	/// Sets last camera in array as an actual camera
	void SetLastCamera() { m_actualCam = m_cameras.end(); --m_actualCam; }
	/// Solves collisions with all objects and world bounds
	bool SolveCollisions(const glm::vec3& pos) const;

private:
	std::list<GameObject*> m_objects; /// Array of all game objects
	std::list<Camera> m_cameras; /// Array of all cameras
	std::vector<Light*> m_lights; /// Array of all lights
	std::map<std::string, Shader*> m_shaders; /// Array of all shaders
	ResourceManager m_resManager; /// Resource manager for loading objects

	// Pointers to interactive objects
	Glider* m_glider; /// Pointer to glider
	Windsock* m_windsock; /// Pointer to windsock
	Pool* m_pool; /// Pointer to pool
	LightDirectional* m_sun; /// Pointer to sun
	LightSpot* m_torchlight; /// Pointer to torchlight

	bool m_keyMap[4]; /// Map of all defined special keyboard buttons
	Time m_time; /// Time structure
	int32_t m_lastTime; /// Elapsed time  detected at the end of last frame
	State m_state; /// Scene state
	Bounds m_bounds; /// Scene ounds

	int32_t m_width; /// Window width
	int32_t m_height; /// Window height
	const std::string configPath = "config.txt"; /// Configuration file path

	std::list<Camera>::iterator m_actualCam; /// Pointer to actual camera

	/** Cycles through camera array
	*/
	void cycleCameras();
	/** Gets all the registered inputs
	*/
	void processInput();
	/** Main program loop - get inputs, update objects
	*/
	void loop();
	/** Load cameras from file
		\param stream File stream to load from
		\return True on success, otherwise false
	*/
	bool loadCameras(std::ifstream& stream);
	/** Load shaders from file
	\param stream File stream to load from
	\return True on success, otherwise false
	*/
	bool loadShaders(std::ifstream& stream);
	/** Load lights from file
	\param stream File stream to load from
	\return True on success, otherwise false
	*/
	bool loadLights(std::ifstream& stream);
	/** Load game objects from file
	\param stream File stream to load from
	\return True on success, otherwise false
	*/
	bool loadGameObjects(std::ifstream& stream);
	/** Destroys all dynamically allocated objects before new initialization.
	*/
	void destroy();
};

#endif __PROGRAM_H