//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ResourceManager.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of resource manager class. It manages object loading and prevents from loading one object multiple times.
*/
//----------------------------------------------------------------------------------------

#include "ResourceManager.h"

using namespace std;

const std::list<SubMesh>& ResourceManager::getObject(const std::string& path, Shader& shader)
{
	std::map<std::string, std::list<SubMesh> >::iterator iter = m_submeshes.find(path);

	if (iter == m_submeshes.end()) { // Not found
		std::list<SubMesh> submeshes;

		if (!loadObject(path, shader, submeshes)) {
			throw LoadingFailedException();
		}


		iter = m_submeshes.insert(make_pair(path, submeshes)).first;
	}

	return iter->second;
}

ResourceManager::ResourceManager()
{
}


ResourceManager::~ResourceManager()
{
	for (std::map<std::string, std::list<SubMesh> >::iterator i = m_submeshes.begin(); i != m_submeshes.end(); ++i) {
		for (std::list<SubMesh>::iterator j = i->second.begin(); j != i->second.end(); ++j)
		{
			glDeleteVertexArrays(1, &(j->geometry.vertexArrayObject));
			glDeleteBuffers(1, &(j->geometry.elementBufferObject));
			glDeleteBuffers(1, &(j->geometry.vertexBufferObject));
			glDeleteTextures(1, &(j->material.texture));
		}
	}
}

bool ResourceManager::loadObject(const std::string& path, Shader& shader, list<SubMesh>& submeshes)
{
	ObjectLoader loader;
	if (!loader.Load(path)) return false;
	list<LoaderMesh> meshes = loader.GetMeshes();

	for (list<LoaderMesh>::iterator i = meshes.begin(); i != meshes.end(); ++i) {
		SubMesh submesh;
		
		glGenBuffers(1, &(submesh.geometry.vertexBufferObject));
		glBindBuffer(GL_ARRAY_BUFFER, submesh.geometry.vertexBufferObject);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*(i->m_numPositions + i->m_numNormals + i->m_numUVs), 0, GL_STATIC_DRAW);

		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float)*i->m_numPositions, i->m_positions);
		glBufferSubData(GL_ARRAY_BUFFER, sizeof(float)*i->m_numPositions, sizeof(float)*i->m_numNormals, i->m_normals);
		glBufferSubData(GL_ARRAY_BUFFER, sizeof(float)*(i->m_numPositions + i->m_numNormals), sizeof(float)*i->m_numUVs, i->m_UVs);

		glGenBuffers(1, &(submesh.geometry.elementBufferObject));
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, submesh.geometry.elementBufferObject);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * i->m_numIndices, i->m_indices, GL_STATIC_DRAW);

		glGenVertexArrays(1, &submesh.geometry.vertexArrayObject);
		glBindVertexArray(submesh.geometry.vertexArrayObject);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, submesh.geometry.elementBufferObject); // bind our element array buffer (indices) to vao
		glBindBuffer(GL_ARRAY_BUFFER, submesh.geometry.vertexBufferObject);

		glEnableVertexAttribArray(shader.positionLocation);
		glVertexAttribPointer(shader.positionLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glEnableVertexAttribArray(shader.normalLocation);
		glVertexAttribPointer(shader.normalLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(float)*i->m_numPositions));

		glEnableVertexAttribArray(shader.UVLocation);
		glVertexAttribPointer(shader.UVLocation, 2, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(float)*(i->m_numPositions + i->m_numNormals)));

		CHECK_GL_ERROR();

		glBindVertexArray(0);

		submesh.material = i->m_material;
		submesh.geometry.numTriangles = i->m_numTriangles;

		submeshes.push_back(submesh);
	}

	return true;
}

