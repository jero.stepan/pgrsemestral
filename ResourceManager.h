//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ResourceManager.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of resource manager class. It manages object loading and prevents from loading one object multiple times.
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "Global.h"
#include "Shader.h"
#include <map>
#include <string>
#include <list>
#include <vector>
#include <iostream>
#include <sstream>

#include "ObjectLoader.h"

/// Object used to store data
class ResourceManager
{
public:
	ResourceManager();
	~ResourceManager();

	/** Returns mesh if it already was loaded, if not loads it from given path
	\param path Path to desired object
	\param shader Shader used to render the object (used here just to map vertex atributes)
	\return List of submeshes of desired mesh
	*/
	const std::list<SubMesh>& getObject(const std::string& path, Shader& shader);

private:
	std::map<std::string, std::list<SubMesh> > m_submeshes; /// List of all loaded meshes

	/** Loads an object in given path
	\param path Path to object
	\param shader Shader used to render the object
	\param submeshes[out] List of submeshes of the loaded object
	\return True on success, otherwise false
	*/
	bool loadObject(const std::string& path, Shader& shader, std::list<SubMesh>& submeshes);
};

