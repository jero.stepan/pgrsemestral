//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Shader.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Partial implementation of shader structure
*/
//----------------------------------------------------------------------------------------

#include "Shader.h"

#include <vector>
#include <fstream>

Shader::~Shader()
{
	pgr::deleteProgramAndShaders(program);
}

bool Shader::Load(const std::string& path)
{
	std::vector<GLuint> shaders;

	std::string vertex = path;
	std::string fragment = path;
	vertex += "Vertex.vs";
	fragment += "Frag.fs";

	GLuint vs = pgr::createShaderFromFile(GL_VERTEX_SHADER, vertex);
	GLuint fs = pgr::createShaderFromFile(GL_FRAGMENT_SHADER, fragment);

	if (!vs || !fs) return false;

	shaders.push_back(vs);
	shaders.push_back(fs);

	program = pgr::createProgram(shaders);

	return program != 0;
}