//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Shader.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of shader structure
*/
//----------------------------------------------------------------------------------------

#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <pgr.h>

/// Basic shader structure
struct Shader
{
	~Shader();

	/** Loads a shader from given path
	\param path Path to shader file
	\return True on success, otherwise false
	*/
	bool Load(const std::string& path);
	/** Initializes shader - finds all uniform locations in shader
	*/
	virtual void Init() = 0;

	GLuint program; /// OpenGL shader program id

	// Vertex attributes
	GLint positionLocation; /// Vertex position location 
	GLint normalLocation; /// Vertex normal location    
	GLint UVLocation; /// Vertex UV location 
};

