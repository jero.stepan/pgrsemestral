//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ShaderControls.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of shader used for rendering Controls game object
*/
//----------------------------------------------------------------------------------------

#include "ShaderControls.h"

void ShaderControls::Init()
{
	ShaderLights::Init();

	// Uniforms
	elapsedTimeLocation = glGetUniformLocation(program, "elapsedTime");
	frameTimeLocation = glGetUniformLocation(program, "frameTime");
	patternLocation = glGetUniformLocation(program, "pattern");

	CHECK_GL_ERROR();
}