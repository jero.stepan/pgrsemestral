//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ShaderControls.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of shader used for rendering Controls game object
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "ShaderLights.h"

/// Shader for drawing Controls game object - used to render dynamic texture (texture atlas)
struct ShaderControls : public ShaderLights
{
	void Init();

	// Uniforms
	GLint elapsedTimeLocation; /// Location of elapsedTime uniform - time from the beginning of the scene
	GLint frameTimeLocation; /// Location of frameTime uniform - duration of a single frame
	GLint patternLocation; /// Location of pattern uniform - pattern of the texture atlas
};

