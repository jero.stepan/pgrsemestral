//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ShaderLights.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of shader structure used for rendering lights
*/
//----------------------------------------------------------------------------------------

#include "ShaderLights.h"

void ShaderLights::Init()
{
	// Vertex attributes
	positionLocation = glGetAttribLocation(program, "position");
	normalLocation = glGetAttribLocation(program, "normal");
	UVLocation = glGetAttribLocation(program, "UVin");

	// Uniforms
	PVMmatrixLocation = glGetUniformLocation(program, "PVMmatrix");
	VMmatrixLocation = glGetUniformLocation(program, "VMmatrix");
	VNmatrixLocation = glGetUniformLocation(program, "VNmatrix");
	texLocation = glGetUniformLocation(program, "tex");
	fogLocation = glGetUniformLocation(program, "fog");
	dayLocation = glGetUniformLocation(program, "day");

	// Material structure
	ambientLocation = glGetUniformLocation(program, "material.ambient");
	diffuseLocation = glGetUniformLocation(program, "material.diffuse");
	specularLocation = glGetUniformLocation(program, "material.specular");
	shininessLocation = glGetUniformLocation(program, "material.shininess");
	useTextureLocation = glGetUniformLocation(program, "material.useTexture");

	// Light structure
	lightAmbientLocation[0] = glGetUniformLocation(program, "sun.ambient");
	lightDiffuseLocation[0] = glGetUniformLocation(program, "sun.diffuse");
	lightSpecularLocation[0] = glGetUniformLocation(program, "sun.specular");
	lightPositionLocation[0] = glGetUniformLocation(program, "sun.position");
	lightAttenuationLocation[0] = glGetUniformLocation(program, "sun.attenuation");

	lightAmbientLocation[1] = glGetUniformLocation(program, "pointlight.ambient");
	lightDiffuseLocation[1] = glGetUniformLocation(program, "pointlight.diffuse");
	lightSpecularLocation[1] = glGetUniformLocation(program, "pointlight.specular");
	lightPositionLocation[1] = glGetUniformLocation(program, "pointlight.position");
	lightAttenuationLocation[1] = glGetUniformLocation(program, "pointlight.attenuation");

	lightAmbientLocation[2] = glGetUniformLocation(program, "torchlight.ambient");
	lightDiffuseLocation[2] = glGetUniformLocation(program, "torchlight.diffuse");
	lightSpecularLocation[2] = glGetUniformLocation(program, "torchlight.specular");
	lightPositionLocation[2] = glGetUniformLocation(program, "torchlight.position");
	lightAttenuationLocation[2] = glGetUniformLocation(program, "torchlight.attenuation");
	lightSpotDirectionLocation[2] = glGetUniformLocation(program, "torchlight.spotDirection");
	lightSpotCosCutOffLocation[2] = glGetUniformLocation(program, "torchlight.spotCosCutOff");
	lightSpotExponentLocation[2] = glGetUniformLocation(program, "torchlight.spotExponent");

	CHECK_GL_ERROR();
}

void ShaderLights::AssignUniforms(const glm::mat4& pvm, const glm::mat4& Vmatrix, const glm::mat4& Mmatrix, const std::vector<Light*>& lights, const State& state)
{
	glUniformMatrix4fv(PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(pvm));
	glm::mat4 VMmatrix = Vmatrix * Mmatrix;
	glm::mat4 mlinear = glm::mat4(glm::mat3(Mmatrix)); // Remove all nonlinear parts from matrix
	glm::mat4 VNmatrix = Vmatrix * glm::transpose(glm::inverse(mlinear));
	glUniformMatrix4fv(VMmatrixLocation, 1, GL_FALSE, glm::value_ptr(VMmatrix));
	glUniformMatrix4fv(VNmatrixLocation, 1, GL_FALSE, glm::value_ptr(VNmatrix));
	glUniform1i(dayLocation, state.day ? 1 : 0);
	glUniform1i(fogLocation, state.fog ? 1 : 0);

	for (uint32_t i = 0; i < maxLights; ++i)
	{
		glm::vec3 position = glm::vec3(Vmatrix * glm::vec4(lights[i]->position, 1.0f));

		glUniform3fv(lightAmbientLocation[i], 1, glm::value_ptr(lights[i]->ambient));
		glUniform3fv(lightDiffuseLocation[i], 1, glm::value_ptr(lights[i]->diffuse));
		glUniform3fv(lightSpecularLocation[i], 1, glm::value_ptr(lights[i]->specular));
		glUniform3fv(lightPositionLocation[i], 1, glm::value_ptr(position));
		glUniform3fv(lightAttenuationLocation[i], 1, glm::value_ptr(lights[i]->attenuation));
	}
	glm::vec3 direction = glm::normalize(glm::vec3(Vmatrix * glm::vec4(lights[2]->spotDirection, 0.0f)));

	glUniform3fv(lightSpotDirectionLocation[2], 1, glm::value_ptr(direction));
	glUniform1f(lightSpotCosCutOffLocation[2], lights[2]->spotCosCutOff);
	glUniform1f(lightSpotExponentLocation[2], lights[2]->spotExponent);
}

void ShaderLights::AssignMaterial(const Material& material)
{
	glUniform3fv(ambientLocation, 1, glm::value_ptr(material.ambient));
	glUniform3fv(diffuseLocation, 1, glm::value_ptr(material.diffuse));
	glUniform3fv(specularLocation, 1, glm::value_ptr(material.specular));
	glUniform1f(shininessLocation, material.shininess);
}