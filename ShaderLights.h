//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ShaderLights.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of shader structure used for rendering lights
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "Global.h"
#include "Shader.h"

/// Structure for lights in shader
struct ShaderLights : public Shader
{
	void Init();
	/** Assigns given parameters to corresponding uniforms
	\param pvm PVM matrix
	\param Vmatrix View matrix
	\param Mmatrix Model matrix
	\param lights List of all lights in the scene
	\param state Scene state - Daylight, fog, etc.
	*/
	void AssignUniforms(const glm::mat4& pvm, const glm::mat4& Vmatrix, const glm::mat4& Mmatrix, const std::vector<Light*>& lights, const State& state);
	/** Assign material to the shader uniforms
	\param material Material of the object
	*/
	void AssignMaterial(const Material& material);

	// Uniforms
	GLint PVMmatrixLocation; /// Location of PVM matrix
	GLint VMmatrixLocation; /// Location of Model View matrix
	GLint VNmatrixLocation; /// Location of Normal View matrix -- transposed model matrix
	GLint texLocation; /// Location of texture sampler
	GLint dayLocation; /// Location of day flag
	GLint fogLocation; /// Location of fog flag

	// Material structure
	GLint diffuseLocation; /// Location of diffuse component
	GLint ambientLocation; /// Location of ambient component
	GLint specularLocation; /// Location of specular component
	GLint shininessLocation; /// Location of shininess component
	GLint useTextureLocation; /// Location of use texture flag - should be always set to 1 - loader does not support models without textures :-(

	// Light structure
	GLint lightAmbientLocation[maxLights]; /// Location of light ambient component
	GLint lightDiffuseLocation[maxLights]; /// Location of light diffuse component
	GLint lightSpecularLocation[maxLights]; /// Location of light specular component
	GLint lightPositionLocation[maxLights]; /// Location of light position
	GLint lightSpotDirectionLocation[maxLights]; /// Location of light spot direction
	GLint lightSpotCosCutOffLocation[maxLights]; /// Location of light cos of cutoff angle
	GLint lightSpotExponentLocation[maxLights]; /// Location of light spot exponent
	GLint lightAttenuationLocation[maxLights]; /// Location of light attenuation
};

