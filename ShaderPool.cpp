//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ShaderPool.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of shader used for rendering Pool game object
*/
//----------------------------------------------------------------------------------------

#include "ShaderPool.h"

void ShaderPool::Init()
{
	ShaderLights::Init();

	//Uniforms
	elapsedTimeLocation = glGetUniformLocation(program, "elapsedTime");
	movementLocation = glGetUniformLocation(program, "movement");

	CHECK_GL_ERROR();
}