//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ShaderPool.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of shader used for rendering Pool game object
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "ShaderLights.h"

/// Shader structure used to render game object Pool
struct ShaderPool : public ShaderLights
{
	void Init();

	// Uniforms
	//GLint PVMmatrixLocation;
	//GLint texLocation;
	GLint elapsedTimeLocation; /// Location of elapsedTime uniform - elapsed time from scene beginning
	GLint movementLocation; /// Location of movement uniform - movement of UVs
};

