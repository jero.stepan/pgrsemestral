//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ShaderSkybox.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of shader used for rendering skybox
*/
//----------------------------------------------------------------------------------------

#include "ShaderSkybox.h"

void ShaderSkybox::Init()
{
	// Vertex attributes
	positionLocation = glGetAttribLocation(program, "position");
	UVLocation = glGetAttribLocation(program, "UVin");

	// Uniforms
	PVMmatrixLocation = glGetUniformLocation(program, "PVM");
	texLocation = glGetUniformLocation(program, "tex");

	CHECK_GL_ERROR();
}