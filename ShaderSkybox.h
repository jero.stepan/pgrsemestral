//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    ShaderSkybox.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of shader used for rendering skybox
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "Shader.h"

/// Shader used to render skybox
struct ShaderSkybox : public Shader
{
	void Init();

	// Uniforms
	GLint PVMmatrixLocation; /// Location of PVM matrix
	GLint texLocation; /// Location of texture sampler
};

