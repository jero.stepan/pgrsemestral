//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Skybox.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of skybox using 6 textures
*/
//----------------------------------------------------------------------------------------

#include "Skybox.h"

Skybox::Skybox(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader) : GameObject(position, scale, rotation, shader)
{
	m_shader = static_cast<ShaderSkybox*>(shader);
}

Skybox::~Skybox()
{
}

void Skybox::Render(const Camera& cam, const std::vector<Light*>& lights, const State& state)
{
	if (state.fog) return;
	glCullFace(GL_FRONT);
	glDisable(GL_DEPTH_TEST);
	glUseProgram(m_shader->program);

	glm::mat4 modelMatrix = glm::rotate(glm::mat4(1.0f), 180.0f, glm::vec3(1, 0, 0));

	glm::mat4 PVM = cam.GetProjectionMatrix() * cam.GetViewMatrix(glm::vec3(0.0f)) * modelMatrix;

	// Uniforms
	glUniformMatrix4fv(m_shader->PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVM));

	std::list<SubMesh>::iterator i = m_submeshes.begin();
	
	glUniform1i(m_shader->texLocation, 0);
	glActiveTexture(getTextureTarget(0));
	glBindTexture(GL_TEXTURE_CUBE_MAP, i->material.texture);

	glBindVertexArray(i->geometry.vertexArrayObject);
	glDrawElements(GL_TRIANGLES, i->geometry.numTriangles * 3, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
	glUseProgram(0);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);

	CHECK_GL_ERROR();
}

void Skybox::Update(float deltaTime)
{

}

bool Skybox::Init(const std::string& path, ResourceManager& manager)
{
	SubMesh submesh;

	glGenTextures(1, &submesh.material.texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, submesh.material.texture);

	for (unsigned int i = 0; i < sidesNum; i++) {
		std::string filename(path + sidesNames[i] + ".png");

		if (!pgr::loadTexImage2D(filename, sidesType[i])) {
			std::cerr << "Failed to load skybox." << std::endl;
			return false;
		}
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	}
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);


	glGenBuffers(1, &submesh.geometry.vertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, submesh.geometry.vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);

	glGenBuffers(1, &submesh.geometry.elementBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, submesh.geometry.elementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(skyboxIndices), skyboxIndices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &submesh.geometry.vertexArrayObject);
	glBindVertexArray(submesh.geometry.vertexArrayObject);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, submesh.geometry.elementBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, submesh.geometry.vertexBufferObject);

	glEnableVertexAttribArray(m_shader->positionLocation);
	glVertexAttribPointer(m_shader->positionLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

	CHECK_GL_ERROR();

	glBindVertexArray(0);
	submesh.geometry.numTriangles = sizeof(skyboxIndices) / (3 * sizeof(unsigned));

	m_submeshes.push_back(submesh);
	return true;
}
