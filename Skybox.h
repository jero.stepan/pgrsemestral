//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Skybox.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of skybox
*/
//----------------------------------------------------------------------------------------

#pragma once

#ifndef __SKYBOX_H
#define __SKYBOX_H

#include <cstdint>
#include "GameObject.h"
#include "ShaderSkybox.h"

const float skyboxVertices[] = {
	-1.0f, -1.0f, -1.0f, // left bottom back
	-1.0f, -1.0f,  1.0f, // left bottom front
	 1.0f, -1.0f,  1.0f, // right bottom front
	 1.0f, -1.0f, -1.0f, // right bottom back
	-1.0f,  1.0f, -1.0f, // left top back
	-1.0f,  1.0f,  1.0f, // left top front
	 1.0f,  1.0f,  1.0f, // right top front
	 1.0f,  1.0f, -1.0f, // right top back
};

//First triangles are rotated CW to pass face culling
const uint32_t skyboxIndices[] = { 2, 1, 0, 0, 3, 2, 0, 1, 5, 5, 4, 0, 1, 2, 6, 5, 1, 6, 2, 3, 7, 2, 7, 6, 3, 0, 4, 4, 7, 3, 4, 5, 6, 6, 7, 4 };

/// Skybox
class Skybox : public GameObject {
public:
	Skybox(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader);
	~Skybox();

	void Render(const Camera& cam, const std::vector<Light*>& lights, const State& state);
	void Update(float deltaTime);
	bool Init(const std::string& path, ResourceManager& manager);
	bool SolveCollisions(const glm::vec3 pos) const { return false; }

private:
	ShaderSkybox* m_shader; /// Shader used form rendering geometry

	const uint32_t sidesNum = 6; /// Number of sides of the cube
	const std::string sidesNames[6] = {"NegX", "PosX", "NegY", "PosY", "NegZ", "PosZ"}; /// Name of each bude side (used for loading textures)
	const GLenum sidesType[6] = { GL_TEXTURE_CUBE_MAP_NEGATIVE_X, GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
								GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, GL_TEXTURE_CUBE_MAP_POSITIVE_Z }; /// OpenGL texture type for each cube side
};

#endif __SKYBOX_H