//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Terrain.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of object Terrain
*/
//----------------------------------------------------------------------------------------

#include "Terrain.h"

Terrain::Terrain(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader) : GameObject(position, scale, rotation, shader)
{
	m_shader = static_cast<ShaderLights*>(shader);
}


Terrain::~Terrain()
{
}

void Terrain::Render(const Camera& cam, const std::vector<Light*>& lights, const State& state)
{
	GameObject::renderObjectLights(cam, m_shader, lights, state);
}

void Terrain::Update(float deltaTime)
{

}

bool Terrain::Init(const std::string& path, ResourceManager& manager)
{
	return GameObject::Load(path, manager);
}