//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Terrain.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of object Terrain
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "GameObject.h"

/// Game object terrain
class Terrain : public GameObject
{
public:
	Terrain(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader);
	~Terrain();

	void Render(const Camera& cam, const std::vector<Light*>& lights, const State& state);
	void Update(float deltaTime);
	bool Init(const std::string& path, ResourceManager& manager);
	bool SolveCollisions(const glm::vec3 pos) const { return false; }

private:
	ShaderLights* m_shader; /// Shader used for rendering the geometry
};

