//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Tree.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of object Tree
*/
//----------------------------------------------------------------------------------------

#include "Tree.h"

Tree::Tree(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader) : GameObject(position, scale, rotation, shader)
{
	m_shader = static_cast<ShaderLights*>(shader);
}

Tree::~Tree()
{
}

void Tree::Render(const Camera& cam, const std::vector<Light*>& lights, const State& state)
{
	glDisable(GL_CULL_FACE);
	GameObject::renderObjectLights(cam, m_shader, lights, state);
	glEnable(GL_CULL_FACE);
}

void Tree::Update(float deltaTime)
{
}

bool Tree::Init(const std::string& path, ResourceManager& manager)
{
	return GameObject::Load(path, manager);
}

bool Tree::SolveCollisions(const glm::vec3 pos) const
{
	return glm::length(pos - m_position) <= radius;
}