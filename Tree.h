//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Tree.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of object Tree
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "GameObject.h"

/// Game object tree
class Tree : public GameObject
{
public:
	Tree(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader);
	~Tree();

	void Render(const Camera& cam, const std::vector<Light*>& lights, const State& state);
	void Update(float deltaTime);
	bool Init(const std::string& path, ResourceManager& manager);
	bool SolveCollisions(const glm::vec3 pos) const;

private:
	ShaderLights* m_shader; /// Shader used for rendering geometry

	const float radius = 10.0f;
};

