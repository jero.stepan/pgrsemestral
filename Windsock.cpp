//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Windsock.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Implementation of object Windsock
*/
//----------------------------------------------------------------------------------------

#include "Windsock.h"

Windsock::Windsock(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader) : GameObject(position, scale, rotation, shader)
{
	m_shader = static_cast<ShaderLights*>(shader);
}


Windsock::~Windsock()
{
}

void Windsock::Render(const Camera& cam, const std::vector<Light*>& lights, const State& state)
{
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glStencilFunc(GL_ALWAYS, id, 0xFF);

	GameObject::renderObjectLights(cam, m_shader, lights, state);

	glDisable(GL_STENCIL_TEST);
}

void Windsock::Update(float deltaTime)
{

}

bool Windsock::Init(const std::string& path, ResourceManager& manager)
{

	return GameObject::Load(path, manager);
}

bool Windsock::SolveCollisions(const glm::vec3 pos) const
{
	return glm::length(pos - m_position) <= radius;
}

void Windsock::Interact()
{
	m_rotation.y += 30.0f;
	if (m_rotation.y > 360.0f) m_rotation.y -= 360.0f;
}