//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    Windsock.h
* \author  Jaroslav Stepan
* \date    2016
* \brief   Definition of object Windsock
*/
//----------------------------------------------------------------------------------------

#pragma once

#include "GameObject.h"

/// Game object windsock
class Windsock : public GameObject
{
public:
	Windsock(glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Shader* shader);
	~Windsock();

	void Render(const Camera& cam, const std::vector<Light*>& lights, const State& state);
	void Update(float deltaTime);
	bool Init(const std::string& path, ResourceManager& manager);
	bool SolveCollisions(const glm::vec3 pos) const;

	/// Interact somehow after picking
	void Interact();

private:
	ShaderLights* m_shader; /// Shader used for rendering geometry

	const uint32_t id = 2; /// ID used in stencil buffer - picking
	const float radius = 6;
};

