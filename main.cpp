//----------------------------------------------------------------------------------------
/** Semestral Project for BI-PGR - Airport
* \file    main.cpp
* \author  Jaroslav Stepan
* \date    2016
* \brief   Main file used for OpenGL, GLUT, Program object initialization  
*/
//----------------------------------------------------------------------------------------

#include <iostream>

#include "Program.h"

const int WIN_WIDTH = 512;
const int WIN_HEIGHT = 512;
const char * WIN_TITLE = "PGR - Semestralka";
const int refreshTimeMs = 16;
Program program; /// Main structure

void onTimer(int) {
	glutPostRedisplay();
	glutTimerFunc(refreshTimeMs, onTimer, 0);

	program.OnTimerEvent();
}

void onReshape(int width, int height) {
	program.OnReshapeEvent(width, height);
}

void onDisplay() {
	program.Render();
}

void onKey(unsigned char key, int, int) {
	program.OnKeyEvent(key);
}

void mouseCallback(int buttonPressed, int buttonState, int mouseX, int mouseY) {
	program.OnMouseEvent(buttonPressed, buttonState, mouseX, mouseY);
}

void mousePassiveCallback(int mouseX, int mouseY) {
	program.OnMousePassiveEvent(mouseX, mouseY);
}

void specialKeyboardUpCallback(int specKeyPressed, int mouseX, int mouseY) {
	bool* keyMap = program.GetKeyMap();

	switch (specKeyPressed) {
	case GLUT_KEY_RIGHT:
		keyMap[K_RIGHT] = false;
		break;
	case GLUT_KEY_LEFT:
		keyMap[K_LEFT] = false;
		break;
	case GLUT_KEY_UP:
		keyMap[K_UP] = false;
		break;
	case GLUT_KEY_DOWN:
		keyMap[K_DOWN] = false;
		break;
	default:
		break;
	}
}

void specialKeyboardDownCallback(int specKeyPressed, int mouseX, int mouseY) {
	bool* keyMap = program.GetKeyMap();

	switch (specKeyPressed) {
	case GLUT_KEY_RIGHT:
		keyMap[K_RIGHT] = true;
		break;
	case GLUT_KEY_LEFT:
		keyMap[K_LEFT] = true;
		break;
	case GLUT_KEY_UP:
		keyMap[K_UP] = true;
		break;
	case GLUT_KEY_DOWN:
		keyMap[K_DOWN] = true;
		break;
	default:
		break;
	}
}

void ProcessMenuEvents(int option) {
	switch (option)
	{
	case 0:
		program.GetState().day = !program.GetState().day;
		break;
	case 1:
		program.GetState().fog = !program.GetState().fog;
		break;
	case 2:
		program.SetFirstCamera();
		break;
	case 3:
		program.SetLastCamera();
		break;
	}
}

int main(int argc, char* argv[]) {
	glutInit(&argc, argv);

	glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
	glutCreateWindow(WIN_TITLE);

	glutDisplayFunc(onDisplay);
	glutReshapeFunc(onReshape);
	glutKeyboardFunc(onKey);
	glutMouseFunc(mouseCallback);
	glutPassiveMotionFunc(mousePassiveCallback);
	glutSpecialFunc(specialKeyboardDownCallback);
	glutSpecialUpFunc(specialKeyboardUpCallback);
	glutTimerFunc(refreshTimeMs, onTimer, 0);

	// Create menu
	int menu = glutCreateMenu(ProcessMenuEvents);

	glutAddMenuEntry("Day/Night", 0);
	glutAddMenuEntry("Toggle fog", 1);
	glutAddMenuEntry("First camera", 2);
	glutAddMenuEntry("Camera on hill", 3);

	glutAttachMenu(GLUT_RIGHT_BUTTON);

	if (!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR)) pgr::dieWithError("pgr init failed, required OpenGL not supported?");

	if (!program.Init(WIN_WIDTH, WIN_HEIGHT)) pgr::dieWithError("init failed, cannot continue");

	glutMainLoop();
	glutDestroyMenu(menu);
	return 0;
}
