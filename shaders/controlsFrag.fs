#version 140

struct Material {
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  float shininess;

  bool useTexture;         
};

struct Light {         
  vec3  ambient;       
  vec3  diffuse;       
  vec3  specular;      
  vec3  position;      
  vec3  spotDirection; 
  float spotCosCutOff; 
  float spotExponent;
  vec3 attenuation;  
};

smooth in vec2 UVV;
smooth in vec3 positionV;
smooth in vec3 normalV;

out vec4 color;
     
uniform Material material;  
uniform sampler2D tex;
uniform bool day;
uniform bool fog;
uniform Light sun;
uniform Light pointlight;
uniform Light torchlight;

uniform mat4 PVMmatrix;
uniform mat4 VMmatrix;
uniform mat4 VNmatrix;

//Shader specific
uniform float elapsedTime;
uniform float frameTime;
uniform vec2 pattern;

vec3 directional(Light light) {
	vec3 lightDirection = normalize(light.position);
	vec3 pointDirection = normalize(-positionV);
	
	vec3 normal = normalize(normalV);
	vec3 reflection = normalize(reflect(-lightDirection, normal));
	
	vec3 result = vec3(0.0f);
	result += material.ambient * light.ambient;
	result += material.diffuse * light.diffuse * max(0.0f, dot(lightDirection, normal));
	
	float specular = max(0.0, dot(reflection, pointDirection));
	result += material.specular * light.specular * pow(specular, material.shininess);
	
	return result;
}

vec3 point(Light light) {
	vec3 lightDirection = light.position - positionV;
	float lightDistance = length(lightDirection);
	lightDirection = normalize(lightDirection);
	
	float attenuation = 1.0f / (light.attenuation.x + light.attenuation.y * lightDistance + light.attenuation.z * lightDistance * lightDistance);
	vec3 normal = normalize(normalV);
	
	vec3 result = vec3(0.0f);
	result += material.ambient * light.ambient;
	result += material.diffuse * light.diffuse * max(0.0f, dot(normal, lightDirection));
	
	vec3 pointDirection = normalize(-positionV);
	vec3 reflection = normalize(reflect(-lightDirection, normal));
	result += pow(max(0.0f, dot(reflection, pointDirection)), material.shininess) * material.specular * light.specular;
	result *= attenuation;
	
	return result;
}

vec3 spot(Light light) {
	vec3 lightDirection = light.position - positionV;
	float lightDistance = length(lightDirection);
	lightDirection = normalize(lightDirection);
	
	float attenuation = 1.0f / (light.attenuation.x + light.attenuation.y * lightDistance + light.attenuation.z * lightDistance * lightDistance);
	vec3 normal = normalize(normalV);
	
	vec3 result = vec3(0.0f);
	
	float cos = dot(-lightDirection, light.spotDirection);
	
	if (cos >= light.spotCosCutOff) {
		vec3 viewDirection = normalize(-positionV);
		vec3 reflection = normalize(reflect(-lightDirection, normal));
		
		result += material.ambient * light.ambient;
		result += material.diffuse * light.diffuse * max(0.0f, dot(normal, lightDirection));
		result += pow(max(0, dot(reflection, viewDirection)), material.shininess) * material.specular * light.specular;
		result *= pow(cos, light.spotExponent);
		result *= attenuation;
	} 
	
	return result;
}

vec4 getFrame(int frame)
{
	vec2 frameSize = vec2(1.0f) / pattern; // size of each frame
	vec2 frameCoord = UVV / pattern; // coords of actual frame
	
	vec2 offset = vec2((frame % int(pattern.x)) * frameSize.x, (frame / pattern.x) * frameSize.y);
	vec2 UVcoord = frameCoord + offset;
	return texture(tex, UVcoord);
}

void main() {

	if (texture(tex,UVV).a < 0.3f) discard;
	
	vec3 globalAmbient = vec3(0.1f);
	
	color = vec4(material.ambient * globalAmbient, 1.0f);
	if (day) 
	{
		color += vec4(directional(sun), 0.0f);
	} else {
		color += vec4(point(pointlight), 0.0f);
		color += vec4(spot(torchlight), 0.0f);
	}
	
	int frame = int(elapsedTime / frameTime);

	color = getFrame(frame) * color;
	
	if(fog)
	{
		vec3 fogColor = vec3(1.0);
		float density = 0.03;
		float dist = length(positionV);
		
		float f = exp(-density * dist);
		color = f * color + (1-f) * fogColor;
	}
}