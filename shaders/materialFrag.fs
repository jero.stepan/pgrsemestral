#version 140

struct Material {
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  float shininess;

  bool useTexture;         
};

uniform sampler2D tex;  
uniform Material material;    

smooth in vec4 colorV;       
smooth in vec2 UVV;     
out vec4 colorF;        

void main() {
  if(material.useTexture) colorF = colorV * texture(tex, UVV);
  else colorF = colorV;
}
