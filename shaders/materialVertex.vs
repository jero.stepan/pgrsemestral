#version 140

struct Material {
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  float shininess;

  bool useTexture;         
};

struct Light {         
  vec3  ambient;       
  vec3  diffuse;       
  vec3  specular;      
  vec3  position;      
  vec3  spotDirection; 
  float spotCosCutOff; 
  float spotExponent;  
};

in vec3 position;           
in vec3 normal;             
in vec2 UVin;   

smooth out vec2 UVV;
smooth out vec4 colorV;        
     
uniform Material material;  
uniform sampler2D tex;
uniform Light spotlight;

uniform mat4 PVMmatrix;
uniform mat4 Vmatrix;
uniform mat4 Mmatrix;
uniform mat4 normalMatrix;

vec4 directionalLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {

	vec3 color = vec3(0.0);

	vec3 L = normalize(light.position);
	vec3 R = reflect(-L, vertexNormal);
	vec3 V = normalize(-vertexPosition);

	color += material.ambient * light.ambient;
	color += max(0, dot(L, vertexNormal)) * material.diffuse * light.diffuse;
	color += pow(max(0, dot(R, V)), material.shininess) * material.specular * light.specular;

  return vec4(color, 1.0);
}

vec4 spotLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {
	
	vec3 color = vec3(0.0);
	
	vec3 L = normalize(vertexPosition - light.position);
	vec3 R = reflect(-L, vertexNormal);
	vec3 V = normalize(-vertexPosition);
	
	vec3 dir = vertexPosition - light.position;
	float cos = max(0, dot(dir, light.spotDirection));
	if (cos > light.spotCosCutOff) cos = 0;
	cos = pow(cos, light.spotExponent);
	
	color += material.ambient * light.ambient;
	color += max(0, dot(L, vertexNormal)) * material.diffuse * light.diffuse;
	color += pow(max(0, dot(R, V)), material.shininess) * material.specular * light.specular;
	color *= cos;
	
	return vec4(color, 1.0);
}

Light sun;

void setupLights() {
  sun.ambient  = vec3(0.0);
  sun.diffuse  = vec3(1.0, 1.0, 0.5f);
  sun.specular = vec3(1.0);
  sun.position = (Vmatrix * vec4(0.0f, 10.0f, 0.0f, 1.0f)).xyz;
}

void main() {

  setupLights();

  vec3 vertexPosition = (Vmatrix * Mmatrix * vec4(position, 1.0)).xyz;
  vec3 vertexNormal   = normalize( (Vmatrix * normalMatrix * vec4(normal, 0.0) ).xyz);

  vec3 globalAmbientLight = vec3(0.3f);
  vec4 outputColor = vec4(material.ambient * globalAmbientLight, 0.0);

  outputColor += directionalLight(sun, material, vertexPosition, vertexNormal);
  outputColor += spotLight(spotlight, material, vertexPosition, vertexNormal);

  gl_Position = PVMmatrix * vec4(position, 1);   // out:v vertex in clip coordinates

  colorV = outputColor;
  UVV = UVin;
}
