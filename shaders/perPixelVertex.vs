#version 140

struct Material {
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  float shininess;

  bool useTexture;         
};

struct Light {         
  vec3  ambient;       
  vec3  diffuse;       
  vec3  specular;      
  vec3  position;      
  vec3  spotDirection; 
  float spotCosCutOff; 
  float spotExponent;
  vec3 attenuation;  
};

in vec3 position;           
in vec3 normal;             
in vec2 UVin;   

smooth out vec2 UVV;
smooth out vec3 positionV;
smooth out vec3 normalV;        
     
uniform Material material;  
uniform sampler2D tex;
uniform bool day;
uniform bool fog;
uniform Light sun;
uniform Light spotlight;
uniform Light torchlight;

uniform mat4 PVMmatrix;
uniform mat4 VMmatrix;
uniform mat4 VNmatrix;

void main() {

  gl_Position = PVMmatrix * vec4(position, 1);

  UVV = UVin;
  
  positionV = (VMmatrix * vec4(position, 1.0f)).xyz;
  normalV = normalize((VNmatrix * vec4(normal, 0.0f)).xyz);
  
}
