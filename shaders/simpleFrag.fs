#version 140

smooth in vec4 col;
out vec4 color;

void main() {
   color = col;
}