#version 140

in vec3 position;
uniform vec3 color;
smooth out vec4 col;

uniform mat4 PVM;

void main() {
   gl_Position = PVM * vec4(position, 1.0f);
   col = vec4(color, 1.0);
}