#version 140

in vec3 UVV;
out vec4 color;

uniform samplerCube tex;

void main() {
	color = texture(tex, UVV);
}