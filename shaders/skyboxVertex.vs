#version 140

in vec3 position;                        
out vec3 UVV;  

uniform mat4 PVM;
uniform samplerCube tex;

void main() {
   gl_Position = PVM * vec4(position, 1.0f);
   UVV = position;
}